﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalApp.Core.Enums
{
    public enum FacetListOrder
    {
        [Display(Name ="Ascending")]
        Ascending,
        [Display(Name = "Descending")]
        Descending,
        [Display(Name = "Relevence")]
        Relevence,
        [Display(Name = "Featured")]
        Featured,
    }
}
