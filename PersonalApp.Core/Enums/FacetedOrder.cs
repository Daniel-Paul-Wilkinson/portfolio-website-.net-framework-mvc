﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalApp.Core.Enums
{
    public enum FacetedOrderEnum
    {
        [Display(Name="Name Ascending")]
        AlphabeticalAscending,
        [Display(Name = "Name Descending")]
        AlphabeticalDescending,
        [Display(Name = "Published Ascending")]
        DateAscending,
        [Display(Name = "Published Descending")]
        DateDescending,
        [Display(Name = "Popularity")]
        Popularity,
    }
}
