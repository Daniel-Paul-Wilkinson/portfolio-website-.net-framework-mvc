﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalApp.Core.Enums
{
    public enum FacetTypeEnum
    {
            [Display(Name = "Brand")]
            Brand,
            [Display(Name = "Design")]
            Design,
            [Display(Name = "Tutorial")]
            Tutorial,
            [Display(Name = "Blog")]
            Blog,
        
    }
}
