﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalApp.Helpers
{
    public class StringHelpers
    {
        public static IEnumerable<string> Split(string url)
        {
            List<string> strings = new List<string>();

            //if the url has text
            if (url != null && url.Length > 0)
            {
                //if the url has multiple prams or one add them to the list
                if (url.Contains(","))
                {
                    strings.AddRange(url.Split(','));
                }
                else
                {
                    strings.Add(url);
                }
            }

            //return list of params
            return strings;
        }
    }
}
