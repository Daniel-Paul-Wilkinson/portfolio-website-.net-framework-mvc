﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebApplication3.Routes
{
    public class FilterRoute : Route
    {
        public FilterRoute(string urlPath, RouteValueDictionary rv)
        : base(
            urlPath + "/{*pathInfo}",rv,
            new MvcRouteHandler()
        )
        {
        }

        public override RouteData GetRouteData(HttpContextBase httpContext)
        {
            var rd = base.GetRouteData(httpContext);
            if (rd == null)
            {
                return null;
            }

            var categoryValue = httpContext.Request.Url.Segments[1].Replace("/", "");

            rd.Values["category.key"] = "category";
            rd.Values["category.value"] = categoryValue;

            var filters = rd.Values["pathInfo"] as string;
            if (!string.IsNullOrEmpty(filters))
            {
                var tokens = filters.Split('/');

                var index = 0;
                for (int i = 0; i < tokens.Length - 1; i += 2)
                {
                    var key = tokens[i];
                    var value = tokens[i + 1];
                    rd.Values[string.Format(key + ".key", index)] = key;
                    rd.Values[string.Format(key + ".value", index)] = value;
                    index++;
                }
            }

            return rd;
        }

    }
}