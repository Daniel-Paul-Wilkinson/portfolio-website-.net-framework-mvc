﻿using PersonalApp.Core;
using PersonalApp.Core.Enums;
using PersonalApp.Helpers;
using PersonalApp.Services.Interfaces;
using PersonalApp.ViewModels.Mail;
using PersonalApp.ViewModels.Menu;
using PersonalApp.ViewModels.NotificationBar;
using PersonalApp.ViewModels.PageVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication3.Controllers
{
    public class NavigationController : Controller
    {

        private readonly IImageService _imageService;
        private readonly ITutorialService _tutorialService;

        public NavigationController(IImageService imageService, ITutorialService tutorialService)
        {
            _imageService = imageService;
            _tutorialService = tutorialService;
        }


        // GET: /<controller>/
        public ActionResult Header()
        {
            var model = new MenuVM()
            {
                MenuCategorys = new List<MenuCategoryVM>()
                {

                    new MenuCategoryVM()
                    {
                        Active = true,

                        CategoryOptions = new List<MenuCategoryOptionsVM>()
                        {

                            new MenuCategoryOptionsVM()
                            {
                                Id = 2,
                                Active = true,
                                Slug = "/Play",
                                Title = "Play",
                            },
                            new MenuCategoryOptionsVM()
                            {
                                Id = 3,
                                Active = true,
                                Slug = "/Design",
                                Title = "Design",
                            },
                            new MenuCategoryOptionsVM()
                            {
                                Id = 3,
                                Active = true,
                                Slug = "/Tutorial/Page/1",
                                Title = "Program",
                            },
                        }
                    }
                }
            };

            return PartialView(model);
        }

        public ActionResult NotificationBar()
        {
            var model = new NotificationBarVM()
            {
                NotificationBarItems = new List<NotificationBarCategoryVM>
                {
                    new NotificationBarCategoryVM()
                    {
                        Active = true,
                        Title = "Blogs",
                        NewCount = 1
                    },
                    new NotificationBarCategoryVM()
                    {
                        Active = true,
                        Title = "Tutorials",
                        NewCount = AsyncHelper.RunSync(()=> _tutorialService.GetTutorialsAsync()).Count,
                    },
                    new NotificationBarCategoryVM()
                    {
                        Active = true,
                        Title = "Designs",
                        NewCount = AsyncHelper.RunSync(()=> _imageService.GetImageType(ImageTypeEnum.Design)).Count(),
                    }
                }
            };

            return PartialView(model);
        }

        public ActionResult SocialBar()
        {
            return PartialView();
        }
        public ActionResult Footer()
        {
            var model = new FooterVM();
            model.Enquiry = new EnquiryVM();
            model.Menu = new MenuVM()
            {
                MenuCategorys = new List<MenuCategoryVM>()
                {
                    //main menu
                    new MenuCategoryVM()
                    {
                        Active = true,
                        Title = "Pages",
                        MenuType = MenuPositionTypeEmum.BothNav,
                        CategoryOptions = new List<MenuCategoryOptionsVM>()
                        {
                            new MenuCategoryOptionsVM()
                            {
                                Id = 1,
                                Active = true,
                                Slug = "/Home/Work",
                                Title = "Work",
                                Selected = true,
                            },
                            new MenuCategoryOptionsVM()
                            {
                                Id = 2,
                                Active = true,
                                Slug = "/Home/Projects",
                                Title = "Projects",
                            },
                            new MenuCategoryOptionsVM()
                            {
                                Id = 3,
                                Active = true,
                                Slug = "/Home/Socials",
                                Title = "Socials",
                            },
                            new MenuCategoryOptionsVM()
                            {
                                Id = 3,
                                Active = true,
                                Slug = "/Home/Tutorial",
                                Title = "Tutorial",
                            },
                            new MenuCategoryOptionsVM()
                            {
                                Id = 3,
                                Active = true,
                                Slug = "/Home/Blog",
                                Title = "Blog",
                            },
                            new MenuCategoryOptionsVM()
                            {
                                Id = 3,
                                Active = true,
                                Slug = "/Home/Hobbies",
                                Title = "Hobbies",
                            }
                        }
                    },
                    new MenuCategoryVM()
                    {
                        Active = true,
                        Title = "Test Links",
                        MenuType = MenuPositionTypeEmum.BothNav,
                        CategoryOptions = new List<MenuCategoryOptionsVM>()
                        {
                            new MenuCategoryOptionsVM()
                            {
                                Id = 1,
                                Active = true,
                                Slug = "/Home/Test",
                                Title = "Work",
                                Selected = true,
                            },
                            new MenuCategoryOptionsVM()
                            {
                                Id = 2,
                                Active = true,
                                Slug = "/Home/Test",
                                Title = "Projects",
                            },
                            new MenuCategoryOptionsVM()
                            {
                                Id = 3,
                                Active = true,
                                Slug = "/Home/Test",
                                Title = "Socials",
                            },
                            new MenuCategoryOptionsVM()
                            {
                                Id = 3,
                                Active = true,
                                Slug = "/Home/Test",
                                Title = "Tutorial",
                            },
                            new MenuCategoryOptionsVM()
                            {
                                Id = 3,
                                Active = true,
                                Slug = "/Home/Test",
                                Title = "Blog",
                            },
                            new MenuCategoryOptionsVM()
                            {
                                Id = 3,
                                Active = true,
                                Slug = "/Home/Test",
                                Title = "Hobbies",
                            }
                        }
                    }
                }
            };

            return PartialView(model);
        }

    }
}