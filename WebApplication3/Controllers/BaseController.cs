﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication3.Controllers
{
    public class BaseController : Controller
    {
        protected void Throw404()
        {
            throw new HttpException(404, "not found");
        }
    }
}