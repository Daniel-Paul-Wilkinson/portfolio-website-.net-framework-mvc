﻿using PersonalApp.Core.Enums;
using PersonalApp.Helpers;
using PersonalApp.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebApplication3.Controllers
{
    public class DesignController : Controller
    {
        private readonly IImageService _imageService;
        private readonly ITutorialService _tutorialService;
        private readonly IFacetService _facetService;
        private readonly IPageService _pageSerive;
        private readonly IConfigurationService _configurationService;


        public DesignController(IImageService imageService, ITutorialService tutorialService, IFacetService facetService, IPageService pageService, IConfigurationService configurationService)
        {
            _imageService = imageService;
            _tutorialService = tutorialService;
            _facetService = facetService;
            _pageSerive = pageService;
            _configurationService = configurationService;
        }

        public int GetValueOrDefault(int number)
        {
            if (number <= 0)
            {
                return 1;
            }
            else
            {
                return number;
            }
        }

        // GET: Hobbies
        public ActionResult Index()
        {
            var order = GetRouteParameter("order");
            var page = GetRouteParameter("page");

            if (page == null)
            {
                page = "1";
            }
            var actualPage = GetValueOrDefault(Convert.ToInt32(page));
            var pageSize = actualPage < 0
                ? int.MaxValue
                : _configurationService.ItemsPerPage;
            var skip = actualPage < 0
                ? 0
                : (actualPage - 1) * pageSize;
            var pageOrder = _facetService.getOrderFromString(order ?? FacetedOrderEnum.AlphabeticalAscending.ToString());
            var model = AsyncHelper.RunSync(() => _imageService.GetDesignsAsync(null, ImageTypeEnum.Design, pageOrder, skip, pageSize));
            model.Order = pageOrder;
            var TotalPages = (int)Math.Ceiling((decimal)model.Count / pageSize);
            model.Pager = new PersonalApp.ViewModels.Pager.PagerVM() { TotalPages = TotalPages, CurrentPage = actualPage };

            model.Type = FacetTypeEnum.Design;

            return View(model);
        }
        [HttpPost]
        public ActionResult Designs()
        {

            //get all route params
            var order = GetRouteParameter("order");
            var page = GetRouteParameter("page");

            //throw 404 if page is null
            if (page == null)
            {
                page = "1";
            }

            //find out our actual page
            var actualPage = GetValueOrDefault(Convert.ToInt32(page));

            //find out our page size
            var pageSize = actualPage < 0
                ? int.MaxValue
                : _configurationService.ItemsPerPage;

            //get the skiped count
            var skip = actualPage < 0
                ? 0
                : (actualPage - 1) * pageSize;

            //get the order
            var pageOrder = _facetService.getOrderFromString(order);

            //get out model which contains products and the over count in the result.
            var model = AsyncHelper.RunSync(() => _imageService.GetDesignsAsync(null, ImageTypeEnum.Design, pageOrder, skip, pageSize));

            //bind model order
            model.Order = pageOrder;

            //get the total pages
            var TotalPages = (int)Math.Ceiling((decimal)model.Count / pageSize);

            //bind and build a new pager based of pages and actual page.
            model.Pager = new PersonalApp.ViewModels.Pager.PagerVM() { TotalPages = TotalPages, CurrentPage = actualPage };

            model.Type = FacetTypeEnum.Design;

            //return tutorials
            return PartialView("_DesignList", model);
        }

        public string GetRouteParameter(string ExpectedParam, RouteData routeData = null)
        {
            RouteData route = null;
            if (routeData == null)
            {
                route = RouteData;
                return (string)route.Values[$"{ExpectedParam}.value"];
            }
            else
            {
                return (string)routeData.Values[$"{ExpectedParam}.value"];
            }

        }
    }



}