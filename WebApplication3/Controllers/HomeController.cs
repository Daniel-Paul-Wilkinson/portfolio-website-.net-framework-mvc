﻿using PersonalApp.Core;
using PersonalApp.Core.Enums;
using PersonalApp.Helpers;
using PersonalApp.Services.Interfaces;
using PersonalApp.ViewModels.Colour;
using PersonalApp.ViewModels.Home;
using PersonalApp.ViewModels.Image;
using PersonalApp.ViewModels.Subject;
using PersonalApp.ViewModels.Tags;
using PersonalApp.ViewModels.Tutorial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace WebApplication3.Controllers
{
    public class HomeController : Controller
    {


        private readonly IImageService _imageService;
        private readonly ITutorialService _tutorialService;

        public HomeController(IImageService imageService, ITutorialService tutorialService)
        {
            _imageService = imageService;
            _tutorialService = tutorialService;
        }

        public ActionResult Index()
        {

            var model = new HomeVM() { };
            model.Work = AsyncHelper.RunSync(() => _imageService.GetImageType(ImageTypeEnum.Brand));
            model.Designs = AsyncHelper.RunSync(() => _imageService.GetImageType(ImageTypeEnum.Design));
            model.Tutorials = AsyncHelper.RunSync(() => _tutorialService.GetTutorialsAsync()).Tutorials;
            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}