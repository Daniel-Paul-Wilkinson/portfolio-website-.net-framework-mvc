﻿using PersonalApp.Core;
using PersonalApp.Core.Enums;
using PersonalApp.Helpers;
using PersonalApp.Services.Interfaces;
using PersonalApp.ViewModels.Facets;
using PersonalApp.ViewModels.PageVM;
using PersonalApp.ViewModels.Tutorial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebApplication3.Controllers
{
    public class TutorialController : BaseController
    {
        private readonly IImageService _imageService;
        private readonly ITutorialService _tutorialService;
        private readonly IFacetService _facetService;
        private readonly IPageService _pageSerive;
        private readonly IConfigurationService _configurationService;


        public TutorialController(IImageService imageService, ITutorialService tutorialService, IFacetService facetService, IPageService pageService, IConfigurationService configurationService)
        {
            _imageService = imageService;
            _tutorialService = tutorialService;
            _facetService = facetService;
            _pageSerive = pageService;
            _configurationService = configurationService;
        }

     
        public ActionResult Index()
        {
            var tags = GetRouteParameter("tags");
            var type = GetRouteParameter("subjects");
            var order = GetRouteParameter("order");
            var page = GetRouteParameter("page");
            var series = GetRouteParameter("series");

            if (page == null)
            {
                page = "1";
            }
            var actualPage = GetValueOrDefault(Convert.ToInt32(page));
            var pageSize = actualPage < 0
                ? int.MaxValue
                : _configurationService.ItemsPerPage;
            var skip = actualPage < 0
                ? 0
                : (actualPage - 1) * pageSize;
            var pageOrder = _facetService.getOrderFromString(order ?? FacetedOrderEnum.AlphabeticalAscending.ToString());  
            var model = AsyncHelper.RunSync(() => _tutorialService.GetTutorialsAsync(null, tags, type,series, pageOrder, skip, pageSize));
            model.Order = pageOrder;
            var TotalPages = (int)Math.Ceiling((decimal)model.Count / pageSize);
            model.Pager = new PersonalApp.ViewModels.Pager.PagerVM() { TotalPages = TotalPages, CurrentPage = actualPage };
            model.Facet = AsyncHelper.RunSync(() => _facetService.GetTutorialFacet(model.Tutorials, tags, type, series));

            model.Type = FacetTypeEnum.Tutorial;


            TempData["controller"] = "tutorial";
            return View(model);
        }

        public int GetValueOrDefault(int number)
        {
            if (number <= 0)
            {
                return 1;
            }
            else
            {
                return number;
            }
        }

        public string GetRouteParameter(string ExpectedParam, RouteData routeData = null)
        {
            RouteData route = null;
            if (routeData == null)
            {
                route = RouteData;
                return (string)route.Values[$"{ExpectedParam}.value"];
            }
            else
            {
                return (string)routeData.Values[$"{ExpectedParam}.value"];
            }

        }

        [HttpPost]
        public ActionResult Tutorials()
        {

            //get all route params
            var tags = GetRouteParameter("tags");
            var type = GetRouteParameter("subjects");
            var order = GetRouteParameter("order");
            var page = GetRouteParameter("page");
            var series = GetRouteParameter("series");

            //throw 404 if page is null
            if (page == null)
            {
                page = "1";
            }

            //find out our actual page
            var actualPage = GetValueOrDefault(Convert.ToInt32(page));

            //find out our page size
            var pageSize = actualPage < 0
                ? int.MaxValue
                : _configurationService.ItemsPerPage;

            //get the skiped count
            var skip = actualPage < 0
                ? 0
                : (actualPage - 1) * pageSize;

            //get the order
            var pageOrder = _facetService.getOrderFromString(order);

            //get out model which contains products and the over count in the result.
            var model = AsyncHelper.RunSync(() => _tutorialService.GetTutorialsAsync(null, tags, type, series, pageOrder, skip, pageSize));

            //bind model order
            model.Order = pageOrder;

            //get the total pages
            var TotalPages = (int)Math.Ceiling((decimal) model.Count / pageSize);

            //bind and build a new pager based of pages and actual page.
            model.Pager = new PersonalApp.ViewModels.Pager.PagerVM() { TotalPages = TotalPages, CurrentPage = actualPage };

            model.Type = FacetTypeEnum.Tutorial;

            //return tutorials
            return PartialView("_TutorialList", model);
        }

        [Route("Tutorial/{id}")]
        public ActionResult TutorialItem(int id) 
{
            TutorialArticleVM vm = AsyncHelper.RunSync(() => _tutorialService.GetAside(id));
            vm.Tutorial = AsyncHelper.RunSync(() => _tutorialService.GetTutorialAsync(id));
            AsyncHelper.RunSync(() => _tutorialService.ReadTutorial(vm.Tutorial));
            return View("_TutorialArticle",vm);
        }
    }
}