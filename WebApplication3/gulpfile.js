﻿/// <binding BeforeBuild='process:sass' ProjectOpened='watch' />
"use strict";

// References: define our package references and also our directory paths.
const gulp = require("gulp"),
    fs = require("fs"),
    sass = require("gulp-sass"),
    imagemin = require("gulp-imagemin"),
    flatten = require('gulp-flatten'),
    concat = require("gulp-concat"),
    cssmin = require("gulp-cssmin"),
    htmlmin = require("gulp-htmlmin"),
    uglify = require("gulp-uglify"),
    merge = require("merge-stream"),
    del = require("del"),
    sourcemaps = require("gulp-sourcemaps"),
    babel = require("gulp-babel"),
    gulpif = require('gulp-if'),
    plumber = require('gulp-plumber'),
    autoprefixer = require('autoprefixer'),
    postcss = require('gulp-postcss'),
    notify = require('gulp-notify');
//---

// Paths
const paths = {
    webroot: "./Content/",
    bower: "./bower_components/",
    lib: "./Content/lib/",
    jsDest: "./Scripts/"
};

paths.cssDest = paths.webroot + "css";

paths.appJsDest = "./Scripts/app/";
paths.appBaseJs = paths.appJsDest + "base/**/*.js";
paths.appServicesJs = paths.appJsDest + "services/**/*.js";
paths.appComponentsJs = paths.appJsDest + "components/**/*.js";

paths.datatablesResponsiveCss = paths.bower + "datatables-responsive/css/";
//---

// Config
//var isDebug = true;
//---

// Tasks
gulp.task("deploy", ["process:sass"]);

gulp.task("process:sass", () => {
    return gulp.src('Styles/site.scss')
        .pipe(plumber({
            errorHandler: function (err) {
                notify.onError({
                    title: "Gulp error in " + err.plugin,
                    message: err.toString()
                })(err);
            }
        }))
        .pipe(sass())
        .pipe(concat('site.css'))
        .pipe(postcss([autoprefixer()]))
        .pipe(gulp.dest(paths.cssDest))
        .pipe(concat('site.min.css'))
        .pipe(postcss([autoprefixer()]))
        .pipe(cssmin())
        .pipe(gulp.dest(paths.cssDest));
});

//gulp.task('process:app:js', () => {
//    return gulp.src([paths.appBaseJs, paths.appServicesJs, paths.appComponentsJs])
//        .pipe(plumber({
//            errorHandler: function (err) {
//                notify.onError({
//                    title: "Gulp error in " + err.plugin,
//                    message: err.toString()
//                })(err);
//            }
//        }))
//        .pipe(babel({
//            presets: ['es2015']
//        }))
//        .pipe(concat('app.js'))
//        .pipe(gulpif(isDebug === false, uglify()))
//        .pipe(gulp.dest(paths.jsDest));
//});

//gulp.task("bower:copy-packages", ["bower:prerequisites"], () => {
//    var bower = {
//        "bootstrap": "bootstrap/dist/**/*.{js,map,css,ttf,svg,woff,woff2,eot}",
//        "jquery": "jquery/dist/jquery.{js,min*}",
//        "jquery-ui": "jquery-ui/jquery-ui.{js,min*}",
//        "jquery-ui-theme": "jquery-ui/themes/base/jquery-ui.{css,min*}",
//        "jquery-validation": "jquery-validation/dist/jquery.validate.{js,min.js}",
//        "jquery-validation-unobtrusive": "jquery-validation-unobtrusive/src/jquery.validate.unobtrusive.js",
//        "datatables": "datatables/media/**/*.{dataTables*,png}",
//        "datatables-responsive": "datatables-responsive/**/*.{responsive.js,css}"
//    };

//    for (var destinationDir in bower) {
//        gulp.src(paths.bower + bower[destinationDir])
//            .pipe(gulp.dest(paths.lib + destinationDir));
//    }
//});

//gulp.task("bower:prerequisites", function () {
//    return gulp.src(paths.datatablesResponsiveCss + '*.dataTables.scss')
//            .pipe(sass())
//            .pipe(gulp.dest(paths.datatablesResponsiveCss));
//});

//---

// Gulp Watch: Define our watch tasks. The watch task for "global-bundle:js" was not picking up the creating and deleting of a new js file in the 'js/global-bundles' folder.
// A solution to this is to use the 'cwd' attribute as stated here: https://stackoverflow.com/a/34346524/5049502
gulp.task("watch", () => {
    gulp.watch('Styles/**/*.scss', ["process:sass"]);
    //gulp.watch(['Scripts/app/base/**/*.js', 'Scripts/app/services/**/*.js', 'Scripts/app/components/**/*.js'], /*{ cwd: paths.webroot },*/["process:app:js"]);
});