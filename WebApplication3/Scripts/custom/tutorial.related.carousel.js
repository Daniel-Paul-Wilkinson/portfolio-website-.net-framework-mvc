﻿$('.standard-carousel').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    arrows: true,
    infinite: true,
    autoplaySpeed: 4000,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: true,
                infinite: false,
                arrows: false,
                dots: false,
                autoplaySpeed: 4000,
            }
        },
        {
            breakpoint: 670,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: true,
                infinite: false,
                arrows: false,
                dots: false,
                autoplaySpeed: 4000,
            }
        },

        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: false,
                autoplay: true,
                arrows: false,
                dots: false,
                autoplaySpeed: 4000,
            }
        }
    ]
});