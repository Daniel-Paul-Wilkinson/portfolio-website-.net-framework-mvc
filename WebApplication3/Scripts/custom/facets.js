﻿//Facet.js - site wide facet helpers.
$(function () {
    GetFacetEvents();
});

//Get the page the facet is currently on.
function GetVariablesFacetPage() {
    return $("#Type").val();
}

//Clear facets
function RefreshFacets() {
    var items = $(".facet-check:checked");
    $.each(items, function (index, item) {
        $(item).prop('checked', false);
    });
}

//1. Decide if the user is on mobile or desktop and use these events.
function GetFacetEvents(page) {

    var page = GetVariablesFacetPage();

    GetSharedEvents(page);

    if ($(window).width() >= 1000) {
        GetDesktopFacetEvents();
    } else {
        GetMobileFacetEvents();
    }
}
function GetSharedEvents(page) {
    //on resize of the screen get desktop or mobile filters
    $(window).on('resize', function () {
        GetFacetEvents(page);
    });

    //https://stackoverflow.com/questions/23166070/
    $(document).on('change', "#facet-order", function () {
        FacetChange(null, false,page);
    });
    //Facet Check Event - important that we targer the element around the checkbox so that we have a more effective search.
    $(document).on("click", ".facet-item, .facet-item label", function (event) {
        //Prevent postback
        //event.stopPropagation();
        console.log('5');

        var chkbox = $(this).find('.facet-check')

        if (chkbox.attr('checked')) {
            chkbox.attr('checked', false);
        } else {
            chkbox.attr('checked', true);
        }

        //facet action
        //--param 1: used to contain the current clicked object
        //--param 2: used to tell if the event is a facet check or not.
        FacetChange(null, true, page);
    });
    //Pagination Event https://stackoverflow.com/questions/2195168/jquery-events-dont-work-with-asp-net-mvc-partial-views
    $(document).on("click", ".pagination li", function (event) {
        //Prevent Postback
        event.preventDefault();
        //facet action
        //--param 1: used to contain the current clicked object (the pagination href link)
        //--param 2: used to tell if the event is a facet check or not.
        FacetChange($(this).find('a').attr('href'), false, page);
    });
    //clear facet event
    $(document).on("click", ".facet-clear", function (event) {
        event.preventDefault();
        RefreshProductList("/"+page+'/page/1');
        RefreshFacets();
        SetURL("/"+page+"/page/1");
    });
}
function GetDesktopFacetEvents() {

    //in desktop view all facets are open
    $('.facet-name').parent('.facet').toggleClass('active');

    //toggle facets in desktop view - can have many open at once.
    $('.facet-name').on('click', function () {
        $(this).parent('.facet').toggleClass('active');
        $(this).parent().find('.facet-items').slideToggle();
    });
}
function GetMobileFacetEvents() {

}

//2 Parse all facets from the page.
function GetFacetSort() {
    //return {name}={value}
    return $('#facet-order').children("option").filter(":selected").text().replace(" ", "")
        + "=" +
        $('#facet-order').children("option").filter(":selected").val();
}
function GetFacetPager(SelectedPage, isFacetEvent) {

    //array used to store URL parts.
    var URL = [];

    //fill array with URL parts by '/'
    URL = $(location).attr('href').split("/");

    //Because we have a URL structure as follows: /tutorials/{key}/{value},{value}/{key}.. we know every 2nd / item is the value, get page value.
    var PageValueIndex = URL.indexOf('page') + 1;

    //if is facet event we need to start at page 1 again - prevents empty pages issue.
    if (isFacetEvent || SelectedPage == null) {
        return "page=" + 1;
    }

    //if the selected page is a new page simply add it.
    if (SelectedPage != null) {
        return "page=" + SelectedPage;
    }

    //if the page clicked is the same simply re-add it.
    if (PageValueIndex != null) {
        return "page=" + URL[PageValueIndex];
    }
}
function GetFacetCheckboxes() {

    //standard array to hold filters
    var filters = [];

    //foreach facet
    $(".facet").each(function () {

        //get the data-key of the group of facets
        var key = $(this).data("key");

        //get the items in each group to match the key
        var items = $(this).find(".facet-check:checked");

        //if we have facets
        if (items.length > 0) {

            //standard temp array to hold val
            var itemValues = [];

            //inner each loop of check options adding them to the temp array
            items.each(function () {
                itemValues.push($(this).val());
            });

            //join the key and grouped facets.
            filters.push(key + "=" + itemValues.join(","));
        }
    });
    return filters;
}

//2.1 Build the URL to post back with.
function SetURL(URL) {
    window.history.pushState(null, document.title, URL);
}
function RebuildURL(Filters, Page, Sort, currPage) {
    //rebuild URL.
    var subjects = [];
    var tags = [];
    var pages = [];
    var series = [];
    if (Page != null) {
        Filters.push(Page);
    }
    $.each(Filters, function (key, value) {
        if (value.indexOf("subjects") != -1) {
            subjects.push(value.split('=')[1]);
        }
        if (value.indexOf("tags") != -1) {
            tags.push(value.split('=')[1]);
        }
        if (value.indexOf("page") != -1) {
            pages.push(value.split('=')[1]);
        }
        if (value.indexOf("series") != -1) {
            series.push(value.split('=')[1]);
        }
    });
    var subjectURL = "";
    var tagURL = "";
    var pageURL = "";
    var orderURL = "";
    var seriesURL = "";

    if (subjects != null && subjects.length > 0) {
        subjectURL = "/subjects/" + subjects.join(",");
    }
    if (tags != null && tags.length > 0) {
        tagURL = "/tags/" + tags.join(",");
    }
    if (pages != null && pages.length > 0) {
        pageURL = "/page/" + pages.join(",");
    }
    if (Sort != null) {
        orderURL = "/order/" + Sort.split("=")[0];
    }
    if (series != null && series.length > 0) {
        seriesURL = "/series/" + series.join(",");
    }
    //set state to the URL address bar.

    SetURL("/" + currPage  + subjectURL + tagURL + seriesURL + orderURL + pageURL);
    return "/" + currPage + "s" + subjectURL + tagURL + seriesURL + orderURL + pageURL;
}
//2.2 Post back with the URL.
function RefreshProductList(url) {
    //post the URL and get the data for the page.
    $.post({
        url: url
    }).done(function (data) {
        $(".faceted-list").html(data);
    });
}

//2.3 Get facets, build url & post url.
function FacetChange(SelectedPage, isFacetEvent, currPage) {

    //get all of the filters e.g { subject=development, tag=MVC }
    var Facets = GetFacetCheckboxes();
    var Page = GetFacetPager(SelectedPage, isFacetEvent);
    var Sort = GetFacetSort();

    //build a URL from the filters and newly selected page
    var URL = RebuildURL(Facets, Page, Sort, currPage);

    //$.Post that URL and dynamically populate the .facet-list tag.
    RefreshProductList(URL);
}






