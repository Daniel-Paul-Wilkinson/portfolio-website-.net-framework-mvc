﻿$('.home-work-carousel').slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    autoplay: true,
    arrows: false,
    infinite: true,
    autoplaySpeed: 4000,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 4,
                autoplay: true,
                infinite: false,
                arrows: false,
                dots: false,
                autoplaySpeed: 4000,
            }
        },
        {
            breakpoint: 670,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                autoplay: true,
                infinite: false,
                arrows: false,
                dots: false,
                autoplaySpeed: 4000,
            }
        },

        {
            breakpoint: 480,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                infinite: false,
                autoplay: true,
                arrows: false,
                dots: false,
                autoplaySpeed: 4000,
            }
        }
    ]
});