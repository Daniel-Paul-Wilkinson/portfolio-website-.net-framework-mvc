﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using System.Web.Http;
using WebApplication3.Models;
using PersonalApp.Core.Interfaces;
using System.Text;

namespace WebApplication3.App_Start
{
    public static class AutofacConfig
    {
        public static void Register()
        {
            try
            {
                var builder = new ContainerBuilder();

                //Website controllers
                builder.RegisterControllers(typeof(MvcApplication).Assembly).PropertiesAutowired();

                builder.RegisterSource(new CustomViewRegistrationSource());

                //Identity objects
                //builder.Register(c => HttpContext.Current.GetOwinContext().Authentication);

                //Now register any depencencies from other assemblies using the IDependencyRegistrar interface


                var registrars = GetAssemblies()
                    .SelectMany(a => a.GetTypes())
                    .Where(t => t.GetInterfaces().Any(i => i == typeof(IDependencyRegistrar)))
                    .Select(t => (IDependencyRegistrar)Activator.CreateInstance(t))
                    .ToList();


                foreach (var registrar in registrars)
                {
                    registrar.Register(builder);
                }

                builder.RegisterModule<AutofacWebTypesModule>();

                var container = builder.Build();
                var mvcResolver = new AutofacDependencyResolver(container);
                DependencyResolver.SetResolver(mvcResolver);

                //GlobalConfiguration.Configuration.UseAutofacActivator(container);

            }
            catch (ReflectionTypeLoadException ex)
            {
                StringBuilder sb = new StringBuilder();
                foreach (Exception exSub in ex.LoaderExceptions)
                {
                    sb.AppendLine(exSub.Message);
                    FileNotFoundException exFileNotFound = exSub as FileNotFoundException;
                    if (exFileNotFound != null)
                    {
                        if (!string.IsNullOrEmpty(exFileNotFound.FusionLog))
                        {
                            sb.AppendLine("Fusion Log:");
                            sb.AppendLine(exFileNotFound.FusionLog);
                        }
                    }
                    sb.AppendLine();
                }
                string errorMessage = sb.ToString();
                //Display or log the error based on your application.
            }
        }

        private static IEnumerable<Assembly> GetAssemblies()
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies().ToList();

            foreach (var dll in Directory.GetFiles(GetBinDirectory(), "*.dll"))
            {
                try
                {
                    var assemblyName = AssemblyName.GetAssemblyName(dll);

                    if (assemblies.Any(a => a.FullName == assemblyName.FullName)) continue;

                    var assembly = AppDomain.CurrentDomain.Load(assemblyName);
                    assemblies.Add(assembly);
                }
                catch (BadImageFormatException)
                {
                    //Ignore this
                }
            }

            return assemblies;
        }

        private static string GetBinDirectory()
        {
            return HostingEnvironment.IsHosted ?
                HttpRuntime.BinDirectory : AppDomain.CurrentDomain.BaseDirectory;
        }
    }
}