﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using WebApplication3.Routes;

namespace WebApplication3
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapMvcAttributeRoutes();

            routes.Add("Tutorial list", new FilterRoute("tutorial", new RouteValueDictionary (new { controller = "Tutorial", action = "Index" })));
            routes.Add("Tutorial lists", new FilterRoute("tutorials", new RouteValueDictionary(new { controller = "Tutorial", action = "Tutorials" })));

            routes.Add("Design list", new FilterRoute("Design", new RouteValueDictionary(new { controller = "Design", action = "Index" })));
            routes.Add("Design lists", new FilterRoute("Designs", new RouteValueDictionary(new { controller = "Design", action = "Designs" })));


            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
