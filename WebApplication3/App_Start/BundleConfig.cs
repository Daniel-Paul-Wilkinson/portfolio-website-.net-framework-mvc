﻿using System.Web;
using System.Web.Optimization;

namespace WebApplication3
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizer/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/main").Include(
                      "~/Scripts/jquery/jquery-{version}.js",
                      "~/Scripts/bootstrap/bootstrap.min.js",
                      "~/Scripts/datatables/jquery.dataTables.min.js",
                      "~/Scripts/slick/slick.min.js",
                      "~/Scripts/custom/home/home.tutorial.carousel.js",
                      "~/Scripts/custom/home/home.previouswork.carousel.js",
                      "~/Scripts/custom/tutorial.related.carousel.js",
                      "~/Scripts/custom/facets.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/css/bootstrap/bootstrap.css",
                      "~/Content/css/site.min.css"));
        }
    }
}
