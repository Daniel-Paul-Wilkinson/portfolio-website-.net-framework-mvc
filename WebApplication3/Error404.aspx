﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Error404.aspx.cs" Inherits="WebApplication3.Error404" %>
<% Response.StatusCode = 404; %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Page not found | Project</title>
    <link href="~/Content/css/bootstrap/bootstrap.css" rel="stylesheet" />
    <link href="~/Content/sass/global.min.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
           <div class="container">
            <div class="row">
                <div class="col-xs-12 error-message">
                    <h1>404 - Page not found</h1>
                    <p>Hmm, it appears the page you are looking for was moved, removed, renamed or doesn't exist anymore.</p>
                    <a href="/">Back Home</a>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
