﻿using Microsoft.AspNet.Identity.EntityFramework;
using PersonalApp.Core;
using PersonalApp.Core.Enums;
using PersonalApp.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalApp.Domain
{
    internal class DataSeeder
    {
        private readonly DataContext _context;

        public DataSeeder(DataContext context)
        {
            _context = context;
        }

        public void Seed()
        {
            try
            {
                //SeedImages();
                //SeedColours();
                //SeedSubject();
                //SeedTags();
                //SeedRoles();
                //SeedTutorials();

            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
        }

        private void SeedColours()
        {
            var colours = new List<Colour>
            {
                //https://htmlcolorcodes.com/color-chart/

                //turquoise
                new Colour()
                {
                    Title="turquoise",
                    HEX="#1abc9c"
                },
                //new Colour()
                //{
                //    Title="turquoise-50",
                //    HEX="#e8f8f5"
                //},
                //new Colour()
                //{
                //    Title="turquoise-100",
                //    HEX="#d1f2eb"
                //},
                //new Colour()
                //{
                //    Title="turquoise-200",
                //    HEX="#a3e4d7"
                //},
                //new Colour()
                //{
                //    Title="turquoise-300",
                //    HEX="#76d7c4"
                //},
                //new Colour()
                //{
                //    Title="turquoise-400",
                //    HEX="#48c9b0"
                //},
                //new Colour()
                //{
                //    Title="turquoise-500",
                //    HEX="#1abc9c"
                //},
                //new Colour()
                //{
                //    Title="turquoise-600",
                //    HEX="#17a589"
                //},
                //new Colour()
                //{
                //    Title="turquoise-700",
                //    HEX="#148f77"
                //},
                //new Colour()
                //{
                //    Title="turquoise-800",
                //    HEX="#117864"
                //},
                //new Colour()
                //{
                //    Title="turquoise-900",
                //    HEX="#0e6251"
                //},

                //green sea
                new Colour()
                {
                    Title="green-sea",
                    HEX="#16a085",
                },
                //new Colour()
                //{
                //    Title="green-sea-50",
                //    HEX="#e8f6f3"
                //},
                //new Colour()
                //{
                //    Title="green-sea-100",
                //    HEX="#d0ece7"
                //},
                //new Colour()
                //{
                //    Title="green-sea-200",
                //    HEX="#a2d9ce"
                //},
                //new Colour()
                //{
                //    Title="green-sea-300",
                //    HEX="#73c6b6"
                //},
                //new Colour()
                //{
                //    Title="turquoise-400",
                //    HEX="#45b39d"
                //},
                //new Colour()
                //{
                //    Title="green-sea-500",
                //    HEX="#16a085"
                //},
                //new Colour()
                //{
                //    Title="green-sea-600",
                //    HEX="#138d75"
                //},
                //new Colour()
                //{
                //    Title="green-sea-700",
                //    HEX="#117a65"
                //},
                //new Colour()
                //{
                //    Title="green-sea-800",
                //    HEX="#0e6655"
                //},
                //new Colour()
                //{
                //    Title="green-sea-900",
                //    HEX="#b5345"
                //},

                //emerald
                new Colour()
                {
                    Title="emerald",
                    HEX="#2ecc71"
                },
                //new Colour()
                //{
                //    Title="emerald-50",
                //    HEX="#eafaf1"
                //},
                //new Colour()
                //{
                //    Title="emerald-100",
                //    HEX="#d5f5e3"
                //},
                //new Colour()
                //{
                //    Title="emerald-200",
                //    HEX="#abebc6"
                //},
                //new Colour()
                //{
                //    Title="emerald-300",
                //    HEX="#82e0aa"
                //},
                //new Colour()
                //{
                //    Title="emerald-400",
                //    HEX="#58d68d"
                //},
                //new Colour()
                //{
                //    Title="emerald-500",
                //    HEX="#2ecc71"
                //},
                //new Colour()
                //{
                //    Title="emerald-600",
                //    HEX="#28b463"
                //},
                //new Colour()
                //{
                //    Title="emerald-700",
                //    HEX="#239b56"
                //},
                //new Colour()
                //{
                //    Title="emerald-800",
                //    HEX="#1d8348"
                //},
                //new Colour()
                //{
                //    Title="emerald-900",
                //    HEX="#186a3b"
                //},

                //nephritis 
                new Colour()
                {
                    Title="nephritis",
                    HEX="#27ae60"
                },
                //new Colour()
                //{
                //    Title="nephritis-50",
                //    HEX="#e9f7ef"
                //},
                //new Colour()
                //{
                //    Title="nephritis-100",
                //    HEX="#d4efdf"
                //},
                //new Colour()
                //{
                //    Title="nephritis-200",
                //    HEX="#a9dfbf"
                //},
                //new Colour()
                //{
                //    Title="nephritis-300",
                //    HEX="#7dcea0"
                //},
                //new Colour()
                //{
                //    Title="nephritis-400",
                //    HEX="#52be80"
                //},
                //new Colour()
                //{
                //    Title="nephritis-500",
                //    HEX="#27ae60"
                //},
                //new Colour()
                //{
                //    Title="nephritis-600",
                //    HEX="#229954"
                //},
                //new Colour()
                //{
                //    Title="nephritis-700",
                //    HEX="#1e8449"
                //},
                //new Colour()
                //{
                //    Title="nephritis-800",
                //    HEX="#196f3d"
                //},
                //new Colour()
                //{
                //    Title="nephritis-900",
                //    HEX="#145a32"
                //},

                //peter river
                new Colour()
                {
                    Title="peter-river",
                    HEX="#3498db"
                },
                //new Colour()
                //{
                //    Title="peter-river-50",
                //    HEX="#ebf5fb"
                //},
                //new Colour()
                //{
                //    Title="peter-river-100",
                //    HEX="#d6eaf8"
                //},
                //new Colour()
                //{
                //    Title="peter-river-200",
                //    HEX="#aed6f1"
                //},
                //new Colour()
                //{
                //    Title="peter-river-300",
                //    HEX="#85c1e9"
                //},
                //new Colour()
                //{
                //    Title="peter-river-400",
                //    HEX="#5dade2"
                //},
                //new Colour()
                //{
                //    Title="peter-river-500",
                //    HEX="#3498db"
                //},
                //new Colour()
                //{
                //    Title="peter-river-600",
                //    HEX="#2e86c1"
                //},
                //new Colour()
                //{
                //    Title="peter-river-700",
                //    HEX="#2874a6"
                //},
                //new Colour()
                //{
                //    Title="peter-river-800",
                //    HEX="#21618c"
                //},
                //new Colour()
                //{
                //    Title="peter-river-900",
                //    HEX="#1b4f72"
                //},

                /* Belize Hole */
                new Colour()
                {
                    Title="belize-hole",
                    HEX="#2980b9"
                },
                //new Colour()
                //{
                //    Title="belize-hole-50",
                //    HEX="#eaf2f8"
                //},
                //new Colour()
                //{
                //    Title="belize-hole-100",
                //    HEX="#d4e6f1"
                //},
                //new Colour()
                //{
                //    Title="belize-hole-200",
                //    HEX="#a9cce3"
                //},
                //new Colour()
                //{
                //    Title="belize-hole-300",
                //    HEX="#7fb3d5"
                //},
                //new Colour()
                //{
                //    Title="belize-hole-400",
                //    HEX="#5499c7"
                //},
                //new Colour()
                //{
                //    Title="belize-hole-500",
                //    HEX="#2980b9"
                //},
                //new Colour()
                //{
                //    Title="belize-hole-600",
                //    HEX="#2471a3"
                //},
                //new Colour()
                //{
                //    Title="belize-hole-700",
                //    HEX="#1f618d"
                //},
                //new Colour()
                //{
                //    Title="belize-hole-800",
                //    HEX="#1a5276"
                //},
                //new Colour()
                //{
                //    Title="belize-hole-900",
                //    HEX="#154360"
                //},

                //amethyst
                new Colour()
                {
                    Title="amethyst",
                    HEX="#9b59b6"
                },
                //new Colour()
                //{
                //    Title="amethyst-50",
                //    HEX="#f5eef8"
                //},
                //new Colour()
                //{
                //    Title="amethyst-100",
                //    HEX="#ebdef0"
                //},
                //new Colour()
                //{
                //    Title="amethyst-200",
                //    HEX="#d7bde2"
                //},
                //new Colour()
                //{
                //    Title="amethyst-300",
                //    HEX="#c39bd3"
                //},
                //new Colour()
                //{
                //    Title="amethyst-400",
                //    HEX="#af7ac5"
                //},
                //new Colour()
                //{
                //    Title="amethyst-500",
                //    HEX="#9b59b6"
                //},
                //new Colour()
                //{
                //    Title="amethyst-600",
                //    HEX="#884ea0"
                //},
                //new Colour()
                //{
                //    Title="amethyst-700",
                //    HEX="#76448a"
                //},
                //new Colour()
                //{
                //    Title="amethyst-800",
                //    HEX="#633974"
                //},
                //new Colour()
                //{
                //    Title="amethyst-900",
                //    HEX="#512e5f"
                //},

                //amethyst
                new Colour()
                {
                    Title="wisteria",
                    HEX="#8e44ad"
                },
                //new Colour()
                //{
                //    Title="wisteria-50",
                //    HEX="#f4ecf7"
                //},
                //new Colour()
                //{
                //    Title="wisteria-100",
                //    HEX="#e8daef"
                //},
                //new Colour()
                //{
                //    Title="wisteria-200",
                //    HEX="#d2b4de"
                //},
                //new Colour()
                //{
                //    Title="wisteria-300",
                //    HEX="#bb8fce"
                //},
                //new Colour()
                //{
                //    Title="wisteria-400",
                //    HEX="#a569bd"
                //},
                //new Colour()
                //{
                //    Title="wisteria-500",
                //    HEX="#8e44ad"
                //},
                //new Colour()
                //{
                //    Title="wisteria-600",
                //    HEX="#7d3c98"
                //},
                //new Colour()
                //{
                //    Title="wisteria-700",
                //    HEX="#6c3483"
                //},
                //new Colour()
                //{
                //    Title="wisteria-800",
                //    HEX="#5b2c6f"
                //},
                //new Colour()
                //{
                //    Title="wisteria-900",
                //    HEX="#4a235a"
                //},

                //wet asphalt
                new Colour()
                {
                    Title="wet-asphalt",
                    HEX="#34495e"
                },
                //new Colour()
                //{
                //    Title="wet-asphalt-50",
                //    HEX="#ebedef"
                //},
                //new Colour()
                //{
                //    Title="wet-asphalt-100",
                //    HEX="#d6dbdf"
                //},
                //new Colour()
                //{
                //    Title="wet-asphalt-200",
                //    HEX="#aeb6bf"
                //},
                //new Colour()
                //{
                //    Title="wet-asphalt-300",
                //    HEX="#85929e"
                //},
                //new Colour()
                //{
                //    Title="wet-asphalt-400",
                //    HEX="#5d6d7e"
                //},
                //new Colour()
                //{
                //    Title="wet-asphalt-500",
                //    HEX="#34495e"
                //},
                //new Colour()
                //{
                //    Title="wet-asphalt-600",
                //    HEX="#2e4053"
                //},
                //new Colour()
                //{
                //    Title="wet-asphalt-700",
                //    HEX="#283747"
                //},
                //new Colour()
                //{
                //    Title="wet-asphalt-800",
                //    HEX="#212f3c"
                //},
                //new Colour()
                //{
                //    Title="wet-asphalt-900",
                //    HEX="#1b2631"
                //},

                //midnight-blue
                new Colour
                {
                    Title="midnight-blue",
                    HEX = "#2c3e50"
                },

                //sunflower
                new Colour
                {
                    Title="sunflower",
                    HEX = "#f1c40f"
                },
                
                //orange
                new Colour
                {
                    Title="orange",
                    HEX = "#f1c40f"
                },

                //carrot
                new Colour
                {
                    Title="carrot",
                    HEX = "#e67e22"
                },

                //pumpkin
                new Colour
                {
                    Title="pumpkin",
                    HEX = "#d35400"
                },

                //alizarin
                new Colour
                {
                    Title="alizarin",
                    HEX = "#e74c3c"
                },

                //alizarin
                new Colour
                {
                    Title="pomegranate",
                    HEX = "#c0392b"
                },

                //clouds
                new Colour
                {
                    Title="clouds",
                    HEX = "#ecf0f1"
                },

                //silver
                new Colour
                {
                    Title="silver",
                    HEX = "#bdc3c7"
                },

                //concrete
                new Colour
                {
                    Title="concrete",
                    HEX = "#95a5a6"
                },

                //asbestos
                new Colour
                {
                    Title="asbestos",
                    HEX = "#7f8c8d"
                },

            };
            colours.ForEach(r => _context.Colours.Add(r));
            _context.SaveChanges();
        }
        private void SeedRoles()
        {
            var roles = new List<IdentityRole>
            {
                new IdentityRole {Name = "Admin"},
                new IdentityRole {Name = "User"}
            };

            roles.ForEach(r => _context.Roles.Add(r));
            _context.SaveChanges();
        }
        private void SeedImages()
        {

            var imgList = new List<Image>()
            {
                new Image()
                {
                    Active = true,
                    Title = "Grainger Games",
                    Path = "GraingerLogo.png",
                    Alt = "Grainger Games Logo",
                    ImageType = (int)ImageTypeEnum.Brand,
                    Url="http://www.graingergames.co.uk/",
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now
                },
                new Image()
                {
                    Active = true,
                    Title = "Berlei",
                    Path = "BerleiLogo.png",
                    Alt = "Berlei Logo",
                    ImageType = (int)ImageTypeEnum.Brand,
                    Url="https://www.berlei.com/",
                                        CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now
                },
                new Image()
                {
                    Active = true,
                    Title = "Chipmunks",
                    Path = "ChipmunksLogo.png",
                    Alt = "Chipmunks Logo",
                    ImageType = (int)ImageTypeEnum.Brand,
                    Url="https://www.chipmunksfootwear.co.uk/"
                },
                new Image()
                {
                    Active = true,
                    Title = "Freestep",
                    Path = "FreestepLogo.png",
                    Alt = "Freestep Logo",
                    ImageType = (int)ImageTypeEnum.Brand,
                    Url="https://www.freestepfootwear.com/"
                },
                new Image()
                {
                    Active = true,
                    Title = "Gossard",
                    Path = "GossardLogo.png",
                    Alt = "Gossard Logo",
                    ImageType = (int)ImageTypeEnum.Brand,
                    Url="https://www.gossard.com/"
                },
                new Image()
                {
                    Active = true,
                    Title = "Hanro",
                    Path = "HanroLogo.png",
                    Alt = "Hanro Logo",
                    ImageType = (int)ImageTypeEnum.Brand,
                    Url="https://shop.hanro.com/"
                },
                new Image()
                {
                    Active = true,
                    Title = "Huber",
                    Path = "HuberLogo.png",
                    Alt = "Huber Logo",
                    ImageType = (int)ImageTypeEnum.Design,
                    Url="http://shop.huber.com/"
                },
                new Image()
                {
                    Active = true,
                    Title = "Skiny",
                    Path = "SkinyLogo.png",
                    Alt = "Skiny Logo",
                    ImageType = (int)ImageTypeEnum.Brand,
                    Url="http://shop.skiny.com/"
                },
                new Image()
                {
                    Active = true,
                    Title = "Hitachi",
                    Path = "HitachiLogo.png",
                    Alt = "Hitachi Logo",
                    ImageType = (int)ImageTypeEnum.Brand,
                    Url="http://www.hitachicm.co.uk/"
                },
                new Image()
                {
                    Active = true,
                    Title = "Inspired",
                    Path = "InspiredLogo.png",
                    Alt = "Inspired Logo",
                    ImageType = (int)ImageTypeEnum.Brand,
                    Url="http://www.inspired.com/"
                },
                new Image()
                {
                    Active = true,
                    Title = "Sytner",
                    Path = "SytnerLogo.png",
                    Alt = "Sytner Logo",
                    ImageType = (int)ImageTypeEnum.Brand,
                    Url="http://www.sytner.com/"
                },
                new Image()
                {
                    Active = true,
                    Title = "Tradecraft",
                    Path = "TradecraftLogo.png",
                    Alt = "Tradecraft Logo",
                    ImageType = (int)ImageTypeEnum.Brand,
                    Url="http://www.Tradecraft.com/"
                },

                                new Image()
                {
                    Active = true,
                    Title = "Dolphin",
                    Path = "case_dolphin.png",
                    Alt = "dolphin case",
                    ImageType = (int)ImageTypeEnum.Design,
                    Url="#"
                },
                new Image()
                {
                    Active = true,
                    Title = "Poem",
                    Path = "case_beautiful.png",
                    Alt = "poem case",
                    ImageType = (int)ImageTypeEnum.Design,
                    Url="#"
                },
                new Image()
                {
                    Active = true,
                    Title = "Trinity",
                    Path = "case_gamer.png",
                    Alt = "gamer case",
                    ImageType = (int)ImageTypeEnum.Design,
                    Url="#"
                },

                new Image()
                {
                    Active = true,
                    Title = "Satalite",
                    Path = "case_satalie.png",
                    Alt = "satalite case",
                    ImageType = (int)ImageTypeEnum.Design,
                    Url="#"
                },


                new Image()
                {
                    Active = true,
                    Title = "IIS Hosting - Lesson 1",
                    Path = "ISS_lesson_1.png",
                    Alt = "ISS Hosting lesson 1",
                    ImageType = (int)ImageTypeEnum.Tutorial,
                    Url="#"
                },
                new Image()
                {
                    Active = true,
                    Title = "MVC - Lesson 1",
                    Path = "MVC_lession_1.png",
                    Alt = "ISS Hosting lesson 1",
                    ImageType = (int)ImageTypeEnum.Tutorial,
                    Url="#"
                },
                new Image()
                {
                    Active = true,
                    Title = "UI - Lesson 1",
                    Path = "UI_lesson_1.png",
                    Alt = "UI lesson 1",
                    ImageType = (int)ImageTypeEnum.Tutorial,
                    Url="#"
                }
            };

            imgList.ForEach(r => _context.Images.Add(r));
            _context.SaveChanges();
        }
        private void SeedTags()
        {
            var lstTags = new List<Tag>()
            {
                new Tag()
                {
                    Title="MVC",
                    Slug = "mvc",
                    fontColourId = 1,
                    BackgroundColourId = 3,
                },
                new Tag()
                {
                    Title=".NET Core",
                    Slug = "net-core",
                    fontColourId = 2,
                    BackgroundColourId = 4,
                },
                new Tag()
                {
                    Title="ISS",
                    Slug = "iss",
                    fontColourId = 2,
                    BackgroundColourId = 4,
                },
            };

            lstTags.ForEach(r => _context.Tags.Add(r));
            _context.SaveChanges();
        }
        private void SeedSubject()
        {
            var tutorials = new List<Subject>
            {
                new Subject()
                {
                    Name = "Development",
                    fontColourId = 1,
                    BackgroundColourId = 4,
                    Slug = "development"
                },
                 new Subject()
                {
                    Name = "Design",
                    fontColourId = 3,
                    BackgroundColourId = 5,
                    Slug = "design"
                },
                 new Subject()
                {
                    Name = "Analysis",
                    fontColourId = 5,
                    BackgroundColourId = 7,
                    Slug = "analysis"
                },
            };
            tutorials.ForEach(r => _context.Subjects.Add(r));
            _context.SaveChanges();
        }
        private void SeedTutorials()
        {
            var tutorials = new List<Tutorial>()
            {
                new Tutorial()
                {
                    Active = true,
                    Content = "<p>This is an article</p>",
                    Extract = "<p>This is a Extract</p>",
                    Title = "ISS Hosting",
                    ImageId = 17,
                    Published = DateTime.Now,
                    SubjectId = 2,
                    Order = 3,
                    Slug = "#",
                    Tags = new List<Tag>()
                    {
                        new Tag()
                        {

                                Title="Tag1",
                            fontColourId = 1,
                            BackgroundColourId = 3,
                        },
                    },
                },
                new Tutorial()
                {
                    Active = true,
                    Content = "<p>This is an article</p>",
                    Extract = "<p>This is a Extract</p>",
                    Title = "MVC Lession 1",
                    ImageId = 18,
                    Published = DateTime.Now,
                    SubjectId = 1,
                    Order = 3,
                    Slug = "#",
                    Tags = new List<Tag>()
                    {
                        new Tag()
                        {

                                Title="PHP",
                            fontColourId = 6,
                            BackgroundColourId = 3,
                        },
                    }
                },
                new Tutorial()
                {
                    Active = true,
                    Content = "<p>This is an article</p>",
                    Extract = "<p>This is a Extract</p>",
                    Title = "UI - Lession 1",
                    ImageId = 19,
                    Published = DateTime.Now,
                    SubjectId = 1,
                    Order = 3,
                    Slug = "#",
                    Tags = new List<Tag>()
                    {
                        new Tag()
                        {
                            Title="ASP.NET",
                            fontColourId = 2,
                            BackgroundColourId = 3,
                        },
                    }
                },
            };
            tutorials.ForEach(r => _context.Tutotials.Add(r));
            _context.SaveChanges();
        }
    }
}
