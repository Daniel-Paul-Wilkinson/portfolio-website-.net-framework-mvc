﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalApp.Domain.Interfaces
{
    public interface IDeletable
    {
        DateTime? DeletedOn { get; set; }

        int? DeletedById { get; set; }
    }
}
