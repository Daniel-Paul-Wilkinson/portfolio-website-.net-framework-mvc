namespace PersonalApp.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedserieslink : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Series",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Slug = c.String(),
                        Title = c.String(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Tutorial", "SeriesId", c => c.Int());
            CreateIndex("dbo.Tutorial", "SeriesId");
            AddForeignKey("dbo.Tutorial", "SeriesId", "dbo.Series", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tutorial", "SeriesId", "dbo.Series");
            DropIndex("dbo.Tutorial", new[] { "SeriesId" });
            DropColumn("dbo.Tutorial", "SeriesId");
            DropTable("dbo.Series");
        }
    }
}
