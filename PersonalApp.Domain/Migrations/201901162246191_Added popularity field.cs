namespace PersonalApp.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addedpopularityfield : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tutorial", "Popularity", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tutorial", "Popularity");
        }
    }
}
