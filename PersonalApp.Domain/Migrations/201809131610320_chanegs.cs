namespace PersonalApp.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class chanegs : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Subject", "Slug", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Subject", "Slug");
        }
    }
}
