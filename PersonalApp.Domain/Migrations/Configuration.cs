namespace PersonalApp.Domain.Migrations
{
    using PersonalApp.Domain.Interfaces;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DataContext>
    {

        public Configuration()
        {
            AutomaticMigrationsEnabled = false;

        }

        protected override void Seed(DataContext context)
        {
            var seeder = new DataSeeder(context);
            seeder.Seed();
        }
    }
}
