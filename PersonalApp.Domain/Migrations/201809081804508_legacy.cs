namespace PersonalApp.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class legacy : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Colours",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        HEX = c.String(),
                        RGB = c.String(),
                        ImageId = c.Int(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Image", t => t.ImageId)
                .Index(t => t.ImageId);
            
            CreateTable(
                "dbo.Image",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Active = c.Boolean(nullable: false),
                        Path = c.String(),
                        Alt = c.String(),
                        Url = c.String(),
                        ImageType = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MenuCategory",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.Int(nullable: false),
                        Slug = c.Int(nullable: false),
                        Active = c.Boolean(nullable: false),
                        Order = c.Int(nullable: false),
                        CategoryOptionsId = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MenuCategoryOptions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CategoryOptionsId = c.Int(nullable: false),
                        Title = c.String(),
                        Slug = c.String(),
                        Active = c.Boolean(nullable: false),
                        Order = c.Int(nullable: false),
                        Selected = c.Boolean(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MenuCategory", t => t.CategoryOptionsId, cascadeDelete: true)
                .Index(t => t.CategoryOptionsId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Subject",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        fontColourId = c.Int(nullable: false),
                        BackgroundColourId = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Colours", t => t.BackgroundColourId)
                .ForeignKey("dbo.Colours", t => t.fontColourId)
                .Index(t => t.fontColourId)
                .Index(t => t.BackgroundColourId);
            
            CreateTable(
                "dbo.Tag",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Slug = c.String(),
                        Title = c.String(),
                        ImageId = c.Int(),
                        fontColourId = c.Int(nullable: false),
                        BackgroundColourId = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Colours", t => t.BackgroundColourId)
                .ForeignKey("dbo.Colours", t => t.fontColourId)
                .ForeignKey("dbo.Image", t => t.ImageId)
                .Index(t => t.ImageId)
                .Index(t => t.fontColourId)
                .Index(t => t.BackgroundColourId);
            
            CreateTable(
                "dbo.Tutorial",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Slug = c.String(),
                        Order = c.Int(nullable: false),
                        Active = c.Boolean(nullable: false),
                        Published = c.DateTime(nullable: false),
                        Title = c.String(),
                        Extract = c.String(),
                        Content = c.String(),
                        ImageId = c.Int(),
                        SubjectId = c.Int(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Image", t => t.ImageId)
                .ForeignKey("dbo.Subject", t => t.SubjectId)
                .Index(t => t.ImageId)
                .Index(t => t.SubjectId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Active = c.Boolean(nullable: false),
                        IsGuest = c.Boolean(nullable: false),
                        LegacyId = c.Int(),
                        FirstName = c.String(maxLength: 50),
                        Surname = c.String(maxLength: 50),
                        Mobile = c.String(maxLength: 20),
                        LastImportedGraingerCreditValue = c.Decimal(precision: 18, scale: 2),
                        GraingerCreditValue = c.Decimal(precision: 18, scale: 2),
                        PasswordLastChanged = c.DateTime(),
                        SubscribedToNewsletter = c.Boolean(nullable: false),
                        CreatedById = c.Int(),
                        DeletedOn = c.DateTime(),
                        DeletedById = c.Int(),
                        CreatedOn = c.DateTime(nullable: false),
                        UpdatedOn = c.DateTime(),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.TutorialTags",
                c => new
                    {
                        TutorialId = c.Int(nullable: false),
                        TagId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.TutorialId, t.TagId })
                .ForeignKey("dbo.Tutorial", t => t.TutorialId, cascadeDelete: true)
                .ForeignKey("dbo.Tag", t => t.TagId, cascadeDelete: true)
                .Index(t => t.TutorialId)
                .Index(t => t.TagId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.TutorialTags", "TagId", "dbo.Tag");
            DropForeignKey("dbo.TutorialTags", "TutorialId", "dbo.Tutorial");
            DropForeignKey("dbo.Tutorial", "SubjectId", "dbo.Subject");
            DropForeignKey("dbo.Tutorial", "ImageId", "dbo.Image");
            DropForeignKey("dbo.Tag", "ImageId", "dbo.Image");
            DropForeignKey("dbo.Tag", "fontColourId", "dbo.Colours");
            DropForeignKey("dbo.Tag", "BackgroundColourId", "dbo.Colours");
            DropForeignKey("dbo.Subject", "fontColourId", "dbo.Colours");
            DropForeignKey("dbo.Subject", "BackgroundColourId", "dbo.Colours");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.MenuCategoryOptions", "CategoryOptionsId", "dbo.MenuCategory");
            DropForeignKey("dbo.Colours", "ImageId", "dbo.Image");
            DropIndex("dbo.TutorialTags", new[] { "TagId" });
            DropIndex("dbo.TutorialTags", new[] { "TutorialId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.Tutorial", new[] { "SubjectId" });
            DropIndex("dbo.Tutorial", new[] { "ImageId" });
            DropIndex("dbo.Tag", new[] { "BackgroundColourId" });
            DropIndex("dbo.Tag", new[] { "fontColourId" });
            DropIndex("dbo.Tag", new[] { "ImageId" });
            DropIndex("dbo.Subject", new[] { "BackgroundColourId" });
            DropIndex("dbo.Subject", new[] { "fontColourId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.MenuCategoryOptions", new[] { "CategoryOptionsId" });
            DropIndex("dbo.Colours", new[] { "ImageId" });
            DropTable("dbo.TutorialTags");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Tutorial");
            DropTable("dbo.Tag");
            DropTable("dbo.Subject");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.MenuCategoryOptions");
            DropTable("dbo.MenuCategory");
            DropTable("dbo.Image");
            DropTable("dbo.Colours");
        }
    }
}
