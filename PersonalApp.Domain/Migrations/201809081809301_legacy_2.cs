namespace PersonalApp.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class legacy_2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Colours", "CreatedAt", c => c.DateTime());
            AlterColumn("dbo.Colours", "UpdatedAt", c => c.DateTime());
            AlterColumn("dbo.Image", "CreatedAt", c => c.DateTime());
            AlterColumn("dbo.Image", "UpdatedAt", c => c.DateTime());
            AlterColumn("dbo.MenuCategory", "CreatedAt", c => c.DateTime());
            AlterColumn("dbo.MenuCategory", "UpdatedAt", c => c.DateTime());
            AlterColumn("dbo.MenuCategoryOptions", "CreatedAt", c => c.DateTime());
            AlterColumn("dbo.MenuCategoryOptions", "UpdatedAt", c => c.DateTime());
            AlterColumn("dbo.Subject", "CreatedAt", c => c.DateTime());
            AlterColumn("dbo.Subject", "UpdatedAt", c => c.DateTime());
            AlterColumn("dbo.Tag", "CreatedAt", c => c.DateTime());
            AlterColumn("dbo.Tag", "UpdatedAt", c => c.DateTime());
            AlterColumn("dbo.Tutorial", "CreatedAt", c => c.DateTime());
            AlterColumn("dbo.Tutorial", "UpdatedAt", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Tutorial", "UpdatedAt", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Tutorial", "CreatedAt", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Tag", "UpdatedAt", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Tag", "CreatedAt", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Subject", "UpdatedAt", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Subject", "CreatedAt", c => c.DateTime(nullable: false));
            AlterColumn("dbo.MenuCategoryOptions", "UpdatedAt", c => c.DateTime(nullable: false));
            AlterColumn("dbo.MenuCategoryOptions", "CreatedAt", c => c.DateTime(nullable: false));
            AlterColumn("dbo.MenuCategory", "UpdatedAt", c => c.DateTime(nullable: false));
            AlterColumn("dbo.MenuCategory", "CreatedAt", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Image", "UpdatedAt", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Image", "CreatedAt", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Colours", "UpdatedAt", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Colours", "CreatedAt", c => c.DateTime(nullable: false));
        }
    }
}
