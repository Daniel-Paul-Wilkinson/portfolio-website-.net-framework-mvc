namespace PersonalApp.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addedchanges : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MenuCategory", "MenuType", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MenuCategory", "MenuType");
        }
    }
}
