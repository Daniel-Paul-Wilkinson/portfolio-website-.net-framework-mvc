﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalApp.Domain.Entity
{
    public class Subject : BaseEntity
    {

        public string Name { get; set; }

        public int fontColourId { get; set; }
        [ForeignKey("fontColourId")]
        public Colour fontColour { get; set; }

        public int BackgroundColourId { get; set; }
        [ForeignKey("BackgroundColourId")]
        public Colour backgroundColour { get; set; }

        public string Slug { get; set; }
    }
}
