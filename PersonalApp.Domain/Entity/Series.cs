﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalApp.Domain.Entity
{
    public class Series : BaseEntity
    {
        public string Slug { get; set; }
        public string Title { get; set; }
    }
}
