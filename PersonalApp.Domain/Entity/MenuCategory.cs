﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalApp.Domain.Entity
{
    public class MenuCategory : BaseEntity
    {
        public int Title { get; set; }
        public int Slug { get; set; }
        public bool Active { get; set; }
        public int Order { get; set; }
        public int CategoryOptionsId { get; set; }
        public int MenuType { get; set; }
        [ForeignKey("CategoryOptionsId")]
        public virtual List<MenuCategoryOptions> CategoryOptions { get; set; }
    }
}
