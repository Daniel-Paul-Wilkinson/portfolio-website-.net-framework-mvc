﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalApp.Domain.Entity
{
    public class Tag : BaseEntity
    {

        public string Slug { get; set; }
        public string Title { get; set; }

        //Tag Icon
        public int? ImageId { get; set; }
        [ForeignKey("ImageId")]
        public Image Image { get; set; }

        //Tag Colour
        public int fontColourId { get; set; }
        [ForeignKey("fontColourId")]
        public Colour fontColour { get; set; }

        public int BackgroundColourId { get; set; }
        [ForeignKey("BackgroundColourId")]
        public Colour backgroundColour { get; set; }

        public virtual List<Tutorial> tutorials { get; set; }

    }
}
