﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalApp.Domain.Entity
{
    public class User : IdentityUser
    {
        public bool Active { get; set; }
        public bool IsGuest { get; set; }
        public int? LegacyId { get; set; }
        [MaxLength(50)]
        public string FirstName { get; set; }
        [MaxLength(50)]
        public string Surname { get; set; }
        [MaxLength(20)]
        public string Mobile { get; set; }
        public decimal? LastImportedGraingerCreditValue { get; set; }
        public decimal? GraingerCreditValue { get; set; }
        public DateTime? PasswordLastChanged { get; set; }
        public bool SubscribedToNewsletter { get; set; }
        public int? CreatedById { get; set; }
        public DateTime? DeletedOn { get; set; }
        public int? DeletedById { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }
}  

