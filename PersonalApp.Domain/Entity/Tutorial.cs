﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PersonalApp.Domain.Entity
{
    public class Tutorial : BaseEntity
    {
        //Develop
        public string Slug { get; set; }
        public int Order { get; set; }
        public bool Active { get; set; }
        public DateTime Published { get; set; }
        //Content
        public string Title { get; set; }
        public string Extract { get; set; }
        public string Content { get; set; }

        //tutorial image
        public int? ImageId { get; set; }
        [ForeignKey("ImageId")]
        public Image Image { get; set; }

        //tutorial subject
        public int? SubjectId { get; set; }
        [ForeignKey("SubjectId")]
        public Subject Subject { get; set; }

        //tutorial tags
        public virtual List<Tag> Tags { get; set; }

        public int Popularity { get; set; }

        //series link
        public int? SeriesId { get; set; }
        [ForeignKey("SeriesId")]
        public Series Series { get; set; }
    }
}