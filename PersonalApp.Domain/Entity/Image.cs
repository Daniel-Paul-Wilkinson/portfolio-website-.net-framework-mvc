﻿using PersonalApp.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalApp.Domain.Entity
{
    public class Image : BaseEntity
    {
        public string Title { get; set; }
        public bool Active { get; set; }
        public string Path { get; set; }
        public string Alt { get; set; }
        public string Url { get; set; }
        public int ImageType { get; set; }
    }
}
