﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalApp.Domain.Entity
{ 
    [Table("Colours")]
    public class Colour : BaseEntity
    { 

        public string Title { get; set; }
        public string HEX { get; set; }
        public string RGB { get; set; }
        //shade image
        public int? ImageId { get; set; }
        [ForeignKey("ImageId")]
        public virtual Image Image { get; set; }
    }
}
