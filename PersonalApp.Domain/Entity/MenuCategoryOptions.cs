﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalApp.Domain.Entity
{
    public class MenuCategoryOptions : BaseEntity
    {
        public int CategoryOptionsId { get; set; }
        public string Title { get; set; }
        public string Slug { get; set; }
        public bool Active { get; set; }
        public int Order { get; set; }
        public bool Selected { get; set; }
    }
}
