﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using PersonalApp.Domain.Entity;
using PersonalApp.Domain.Interfaces;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace PersonalApp.Domain
{

    public class DataContext : IdentityDbContext<User>, IDataContext
    {
        public DataContext() : base("DefaultConnection")
        { }

        //GENERAL
        //images => nothing
        public DbSet<Image> Images { get; set; }

        //Colours => Images
        public DbSet<Colour> Colours { get; set; }

        //Subjects => Colours
        public DbSet<Subject> Subjects { get; set; }
        
        public DbSet<Series> Series { get; set; }

        //Tags => Colours, Images
        public DbSet<Tag> Tags { get; set; }

        //CONTENT
        //Tutorials => Images, Colours, Tags, Subject
        public DbSet<Tutorial> Tutotials { get; set; }

        //MENU
        //MenuCategoryOptions = > nothing
        public DbSet<MenuCategoryOptions> MenuCategoryOptions { get; set; }

        //Menu Category => MenuCategoryOptions
        public DbSet<MenuCategory> MenuCategories { get; set; }

        #region Save Changes
        public override int SaveChanges()
        {
            
                foreach (var entityEntry in ChangeTracker.Entries())
                {
                    //Update the UpdatedOn property automagically
                    if (entityEntry.State == EntityState.Modified && entityEntry.Entity is IAuditable)
                    {
                        var auditableEntity = (IAuditable)entityEntry.Entity;
                        auditableEntity.UpdatedOn = DateTime.UtcNow;
                    }

                    //Update the DeletedOn and IsDeleted properties automagically
                    if (entityEntry.State == EntityState.Deleted && entityEntry.Entity is IDeletable)
                    {
                        var deletableEntity = (IDeletable)entityEntry.Entity;
                        deletableEntity.DeletedOn = DateTime.UtcNow;

                        //Don't delete it any more
                        entityEntry.State = EntityState.Modified;
                    }
                }

            try
            {
                return base.SaveChanges();
            }
            catch (FormattedDbEntityValidationException ex)
            {
                Console.Write(ex);
                return 1;
    
            }
        }
        public override async Task<int> SaveChangesAsync()
        {
            foreach (var entityEntry in ChangeTracker.Entries())
            {
                //Update the UpdatedOn property automagically
                if (entityEntry.State == EntityState.Modified && entityEntry.Entity is IAuditable)
                {
                    var auditableEntity = (IAuditable)entityEntry.Entity;
                    auditableEntity.UpdatedOn = DateTime.UtcNow;
                }

                //Update the DeletedOn and IsDeleted properties automagically
                if (entityEntry.State == EntityState.Deleted && entityEntry.Entity is IDeletable)
                {
                    var deletableEntity = (IDeletable)entityEntry.Entity;
                    deletableEntity.DeletedOn = DateTime.UtcNow;

                    //Don't delete it any more
                    entityEntry.State = EntityState.Modified;
                }
            }

            return await base.SaveChangesAsync();
        }
        public int SaveChanges(bool forceDelete)
        {
            return forceDelete ? base.SaveChanges() : SaveChanges();
        }
        public async Task<int> SaveChangesAsync(bool forceDelete)
        {
            return forceDelete ?
                await base.SaveChangesAsync() :
                await SaveChangesAsync();
        }
        #endregion

        #region onchange
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<Tag>()
                    .HasRequired(c => c.backgroundColour)
                    .WithMany()
                    .WillCascadeOnDelete(false);
            modelBuilder.Entity<Tag>()
                .HasRequired(c => c.fontColour)
                .WithMany()
                .WillCascadeOnDelete(false);


            modelBuilder.Entity<Subject>()
                 .HasRequired(c => c.backgroundColour)
                 .WithMany()
                 .WillCascadeOnDelete(false);
            modelBuilder.Entity<Subject>()
                .HasRequired(c => c.fontColour)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Tutorial>()
                .HasMany(c => c.Tags)
                .WithMany(p => p.tutorials)
                .Map(
                m =>
                {
                    m.MapLeftKey("TutorialId");
                    m.MapRightKey("TagId");
                    m.ToTable("TutorialTags");
                });
        }
    

    public DbContextTransaction BeginTransaction()
    {
        return Database.BeginTransaction();
    }

    public void Configure(bool lazyLoading, bool proxyCreation)
    {
        Configuration.LazyLoadingEnabled = true;
        Configuration.ProxyCreationEnabled = true;
    }
    #endregion
}
}
