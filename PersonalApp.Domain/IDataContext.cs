﻿using Microsoft.AspNet.Identity.EntityFramework;
using PersonalApp.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalApp.Domain
{
    public interface IDataContext
    {
        IDbSet<User> Users { get; }
        IDbSet<IdentityRole> Roles { get; }

        //GENERAL
        //images => nothing
         DbSet<Image> Images { get; set; }

        //Colours => Images
         DbSet<Colour> Colours { get; set; }

        //Subjects => Colours
         DbSet<Subject> Subjects { get; set; }

        //Tags => Colours, Images
         DbSet<Tag> Tags { get; set; }

         DbSet<Series> Series { get; set; }
 

        //CONTENT
        //Tutorials => Images, Colours, Tags, Subject
        DbSet<Tutorial> Tutotials { get; set; }

        //MENU
        //MenuCategoryOptions = > nothing
         DbSet<MenuCategoryOptions> MenuCategoryOptions { get; set; }

        //Menu Category => MenuCategoryOptions
         DbSet<MenuCategory> MenuCategories { get; set; }


        Task<int> SaveChangesAsync();
        int SaveChanges();
    }
}
