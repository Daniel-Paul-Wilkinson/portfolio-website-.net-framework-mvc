﻿using PersonalApp.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalApp.ViewModels.Facets
{
    public class FacetVM : BaseVM
    {
        public DateTime? MinDate { get; set; }
        public DateTime? MaxDate { get; set; }
        public string SortBy { get; set; }
        public string SearchText { get; set; }
        public List<FacetCategoryVM> FacetCategorys { get; set; }
    }
}
