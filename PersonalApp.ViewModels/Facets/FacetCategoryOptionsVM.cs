﻿using PersonalApp.ViewModels.Base;

namespace PersonalApp.ViewModels.Facets
{
    public class FacetCategoryOptionsVM : BaseVM
    {
        public bool Active { get; set; }
        public string Slug { get; set; }
        public string DisplayName { get; set; }
    }
}