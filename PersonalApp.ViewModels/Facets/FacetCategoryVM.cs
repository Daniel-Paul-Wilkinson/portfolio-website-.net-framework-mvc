﻿using PersonalApp.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalApp.ViewModels.Facets
{
    public class FacetCategoryVM : BaseVM
    {
        public bool Active { get; set; }
        public string Slug { get; set; }
        public string DisplayName { get; set; }
        public List<FacetCategoryOptionsVM> FacetCategoryOptionsVMs {get; set;}
    }
}
