﻿using PersonalApp.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PersonalApp.ViewModels.Menu
{
    public class MenuVM : BaseVM
    {
        public IEnumerable<MenuCategoryVM> MenuCategorys { get; set; }
    }

}