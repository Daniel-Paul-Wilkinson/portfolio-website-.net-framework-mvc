﻿using PersonalApp.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PersonalApp.ViewModels.Menu
{
    public class MenuCategoryOptionsVM : BaseVM
    {
        public string Title { get; set; }
        public string Slug { get; set; }
        public bool Active { get; set; }
        public int Order { get; set; }
        public bool Selected { get; set; }
    }
}