﻿using PersonalApp.ViewModels.Base;
using PersonalApp.ViewModels.Tutorial;
using System.Collections.Generic;

namespace PersonalApp.ViewModels.PageVM
{
    public class TutorialPageVM : BaseFacetItemVMm
    {
        public IEnumerable<TutorialVM> Tutorials { get; set; }
    }
}
