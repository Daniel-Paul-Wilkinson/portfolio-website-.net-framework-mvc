﻿using PersonalApp.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalApp.ViewModels.Tutorial
{
    public class TutorialArticleAsideVM
    {
        public TutorialTypeEnum TutorialArticalAsideType { get; set; }
        public IEnumerable<TutorialVM> RealatedTutorials { get; set; }
    }
}
