﻿using PersonalApp.ViewModels.Base;
using PersonalApp.ViewModels.Image;
using PersonalApp.ViewModels.Series;
using PersonalApp.ViewModels.Subject;
using PersonalApp.ViewModels.Tags;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace PersonalApp.ViewModels.Tutorial
{
    public class TutorialVM : BaseVM
    {
        public string Slug { get; set; }
        public int Order { get; set; }
        public bool Active { get; set; }
        public DateTime Published { get; set; }
        public string Title { get; set; }
        public string Extract { get; set; }
        public string Content { get; set; }
        public int? ImageId { get; set; }
        public ImageVM Image { get; set; }
        public int? SubjectId { get; set; }
        public SubjectVM Subject { get; set; }
        public List<TagVM> Tags { get; set; }
        public int? SeriesId { get; set; }
        public SeriesVM Series { get; set; }
        public int Popularity { get; set; }
    }
}