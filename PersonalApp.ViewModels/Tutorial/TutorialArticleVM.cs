﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalApp.ViewModels.Tutorial
{
    public class TutorialArticleVM
    {
        public TutorialVM Tutorial { get; set; }
        public IEnumerable<TutorialArticleAsideVM> Aside { get; set; }
    }
}
