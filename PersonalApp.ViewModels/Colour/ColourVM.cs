﻿
using PersonalApp.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PersonalApp.ViewModels.Colour
{
    public class ColourVM : BaseVM
    {
        public string Title { get; set; }
        public string HEX { get; set; }
        public string RGB { get; set; }
    }
}