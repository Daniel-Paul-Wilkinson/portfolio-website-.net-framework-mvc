﻿using PersonalApp.Core;
using PersonalApp.Core.Enums;
using PersonalApp.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PersonalApp.ViewModels.Image
{
    public class ImageVM : BaseVM
    {
        public string Title { get; set; }
        public bool Active { get; set; }
        public string Path { get; set; }
        public string Alt { get; set; }
        public string Url { get; set; }
        public ImageTypeEnum ImageType { get; set; }
    }
}