﻿using PersonalApp.ViewModels.Base;
using PersonalApp.ViewModels.Colour;
using PersonalApp.ViewModels.Image;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace PersonalApp.ViewModels.Tags
{
    public class TagVM : BaseVM
    {
        public string Slug { get; set; }
        public string Title { get; set; }
        public int? ImageId { get; set; }
        public ImageVM Image { get; set; }
        public int fontColourId { get; set; }
        public ColourVM fontColour { get; set; }
        public int BackgroundColourId { get; set; }
        public ColourVM backgroundColour { get; set; }
    }
}