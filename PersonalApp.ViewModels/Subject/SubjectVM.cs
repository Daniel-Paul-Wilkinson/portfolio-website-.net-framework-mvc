﻿using PersonalApp.ViewModels.Base;
using PersonalApp.ViewModels.Colour;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PersonalApp.ViewModels.Subject
{
    public class SubjectVM : BaseVM
    {
        public string Name { get; set; }
        public int fontColourId { get; set; }
        public ColourVM fontColour { get; set; }
        public int BackgroundColourId { get; set; }
        public ColourVM backgroundColour { get; set; }
        public string Slug { get; set; }
    }
}