﻿using PersonalApp.Core.Enums;
using PersonalApp.ViewModels.Facets;
using PersonalApp.ViewModels.Pager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalApp.ViewModels.Base
{
    public class BaseFacetItemVMm : BaseVM
    {
        public FacetVM Facet { get; set; }
        public PagerVM Pager { get; set; }
        public int Count { get; set; }
        public FacetedOrderEnum Order { get; set; }
        public FacetTypeEnum Type { get; set; }
    }
}
