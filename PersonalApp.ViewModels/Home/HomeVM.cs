﻿using PersonalApp.ViewModels.Base;
using PersonalApp.ViewModels.Image;
using PersonalApp.ViewModels.Tutorial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PersonalApp.ViewModels.Home
{
    public class HomeVM : BaseVM
    {
        public IEnumerable<TutorialVM> Tutorials { get; set; }
        public IEnumerable<ImageVM> Work { get; set; }
        public IEnumerable<ImageVM> Designs { get; set; }
    }
}