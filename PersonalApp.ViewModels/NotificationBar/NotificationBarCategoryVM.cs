﻿using PersonalApp.ViewModels.Base;
using PersonalApp.ViewModels.Image;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PersonalApp.ViewModels.NotificationBar
{
    public class NotificationBarCategoryVM : BaseVM
    {
        public string Title { get; set; }
        public string Slug { get; set; }
        public ImageVM Image { get; set; }
        public bool Active { get; set; }
        public int NewCount { get; set; }
    }
}