﻿using PersonalApp.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PersonalApp.ViewModels.NotificationBar
{
    public class NotificationBarVM : BaseVM
    {
        public string ForgroundColour { get; set; }
        public bool Active { get; set; } 
        public string BackgroundColour { get; set; }
        public IEnumerable<NotificationBarCategoryVM> NotificationBarItems { get; set; }
    }
}