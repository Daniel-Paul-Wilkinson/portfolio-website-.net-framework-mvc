﻿using PersonalApp.Core.Enums;
using PersonalApp.ViewModels.Base;
using PersonalApp.ViewModels.Facets;
using PersonalApp.ViewModels.Image;
using PersonalApp.ViewModels.Pager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalApp.ViewModels.Design
{
    public class DesignPageVM : BaseFacetItemVMm
    {
        public IEnumerable<ImageVM> FacetItems { get; set; }
    }
}
