﻿using PersonalApp.ViewModels.Mail;
using PersonalApp.ViewModels.Menu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalApp.ViewModels.PageVM
{
    public class FooterVM
    {
      public MenuVM Menu { get; set; }
      public EnquiryVM Enquiry { get; set; }
    }
}
