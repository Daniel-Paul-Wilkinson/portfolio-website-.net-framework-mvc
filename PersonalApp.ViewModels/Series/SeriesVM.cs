﻿using PersonalApp.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalApp.ViewModels.Series
{
    public class SeriesVM : BaseVM
    {
        public string Slug { get; set; }
        public string Title { get; set; }
    }
}
