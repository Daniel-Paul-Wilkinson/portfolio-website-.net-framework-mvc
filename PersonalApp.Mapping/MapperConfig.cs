﻿using AutoMapper;
using PersonalApp.Domain.Entity;
using PersonalApp.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalApp.Mapper
{
    public static class MapperConfig
    {
        private static IMapper _mapper;

        private static IMapper Mapper
        {
            get
            {
                return _mapper ??
                    (_mapper = new MapperConfiguration(config =>
                    {
                        //MapUsers(config);
                        //MapPages(config);
                        //MapProducts(config);
                        //MapProductAttributes(config);
                        //MapCategories(config);
                        //MapImages(config);
                        //MapBasket(config);
                        //MapLandingPages(config);
                        //MapEntities(config);
                        //MapGenericModels(config);
                        //MapCarousels(config);
                        //MapProductCollections(config);
                        //MapStores(config);
                        //MapDiscounts(config);
                        //MapGenericModels(config);
                    }).CreateMapper());
            }
        }
        public static TOutput ToViewModel<TOutput>(this BaseEntity input)
        {
            return Mapper.Map<BaseEntity, TOutput>(input);
        }
        public static IEnumerable<TOutput> ToViewModel<TOutput>(this IEnumerable<BaseEntity> input)
        {
            return Mapper.Map<IEnumerable<BaseEntity>, IEnumerable<TOutput>>(input);
        }
        public static IList<TOutput> ToViewModel<TOutput>(this IList<BaseEntity> input)
        {
            return Mapper.Map<IList<BaseEntity>, IList<TOutput>>(input);
        }

        public static TOutput ToViewModel<TOutput>(this BaseVM input)
        {
            return Mapper.Map<BaseVM, TOutput>(input);
        }
    }
}
