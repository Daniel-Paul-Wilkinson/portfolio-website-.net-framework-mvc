﻿using PersonalApp.Domain;
using PersonalApp.Mapper;
using PersonalApp.Services.Interfaces;
using PersonalApp.ViewModels.Facets;
using PersonalApp.ViewModels.Subject;
using PersonalApp.ViewModels.Tutorial;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalApp.Services.Services
{
    public class SubjectService : ISubjectService
    {
        private readonly IDataContext _context;
        private readonly IImageService _imageService;
        public SubjectService(IDataContext context, IImageService imageService)
        {
            _context = context;
            _imageService = imageService;
        }

        public async Task<IEnumerable<SubjectVM>> GetSubjectsAsync()
        {
            var tutorials = await _context.Subjects
                .ToListAsync();
            return tutorials.ToViewModel<SubjectVM>()
                .Distinct();
        }
        public async Task<FacetCategoryVM> GetSubjectFacetCategory(IEnumerable<TutorialVM> tutorials, IEnumerable<string> activeSubjects)
        {
            List<FacetCategoryOptionsVM> facetCategoryOptionsVMs = new List<FacetCategoryOptionsVM>();

            var subjects = await GetSubjectsAsync();

            foreach(var subject in subjects)
            {
                facetCategoryOptionsVMs.Add(new FacetCategoryOptionsVM()
                {
                   Active = (activeSubjects.Contains(subject.Slug)),
                   Slug = subject.Slug,
                   DisplayName = subject.Name,
                   CreatedAt = subject.CreatedAt,
                   UpdatedAt = subject.UpdatedAt,
                   Id = subject.Id,
                });
            }

            return new FacetCategoryVM {
                FacetCategoryOptionsVMs = facetCategoryOptionsVMs,
                DisplayName = "Subjects",
                Active = true,
                Slug = "subjects",
                Id = 0,
            };

        }

    }
}
