﻿using PersonalApp.Core;
using PersonalApp.Core.Enums;
using PersonalApp.Domain;
using PersonalApp.Helpers;
using PersonalApp.Mapper;
using PersonalApp.Services.Interfaces;
using PersonalApp.ViewModels.Facets;
using PersonalApp.ViewModels.Image;
using PersonalApp.ViewModels.Tutorial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Routing;

namespace PersonalApp.Services.Services
{
    public class FacetService : IFacetService
    {

        private readonly IDataContext _context;
        private readonly ITutorialService _tutorialService;
        private readonly ITagService _tagService;
        private readonly ISubjectService _subjectService;
        private readonly ISeriesService _seriesService;


        public FacetService(IDataContext context, ITutorialService tutorialService,
            ITagService tagService, ISubjectService subjectService, ISeriesService seriesService)
        {
            _context = context;
            _tutorialService = tutorialService;
            _tagService = tagService;
            _subjectService = subjectService;
            _seriesService = seriesService;
        }




        public async Task<FacetVM> GetTutorialFacet(IEnumerable<TutorialVM> tutorials, string tags, string type, string series)

        {

            var activeTags = StringHelpers.Split(tags);
            var activeSubjects = StringHelpers.Split(type);
            var activeSeries = StringHelpers.Split(series);


            return new FacetVM()
            {

                FacetCategorys = new List<FacetCategoryVM>()
                {
                    await _subjectService.GetSubjectFacetCategory(tutorials,activeSubjects),
                    await _tagService.GetTagFacetCategory(tutorials,activeTags),
                    await _seriesService.GetSeriesCategory(tutorials,activeSeries)
                },
            };
        }
        public FacetedOrderEnum getOrderFromString(string order)
        {
            FacetedOrderEnum enumOrder = FacetedOrderEnum.AlphabeticalAscending;

            switch (order)
            {
                case "NameAscending":
                    return enumOrder;
                case "NameDescending":
                    return FacetedOrderEnum.AlphabeticalDescending;
                case "PublishedAscending":
                    return FacetedOrderEnum.DateAscending;
                case "PublishedDescending":
                    return FacetedOrderEnum.DateDescending;
                case "Popularity":
                    return FacetedOrderEnum.Popularity;
            }
            return enumOrder;
        }
    }
}
