﻿using PersonalApp.Domain;
using PersonalApp.Mapper;
using PersonalApp.Services.Interfaces;
using PersonalApp.ViewModels.Facets;
using PersonalApp.ViewModels.Series;
using PersonalApp.ViewModels.Subject;
using PersonalApp.ViewModels.Tutorial;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalApp.Services.Services
{
    public class SeriesService : ISeriesService
    {


        private readonly IDataContext _context;
 
        public SeriesService(IDataContext context)
        {
            _context = context;

        }

        public async Task<IEnumerable<SeriesVM>> GetSeriesAsync()
        {
            var tutorials = await _context.Series
                .ToListAsync();
            return tutorials.ToViewModel<SeriesVM>()
                .Distinct();
        }
        public async Task<FacetCategoryVM> GetSeriesCategory(IEnumerable<TutorialVM> tutorials, IEnumerable<string> activeSubjects)
        {
            List<FacetCategoryOptionsVM> facetCategoryOptionsVMs = new List<FacetCategoryOptionsVM>();

            var subjects = await GetSeriesAsync();

            foreach (var subject in subjects)
            {
                facetCategoryOptionsVMs.Add(new FacetCategoryOptionsVM()
                {
                    Active = (activeSubjects.Contains(subject.Slug)),
                    Slug = subject.Slug,
                    DisplayName = subject.Title,
                    CreatedAt = subject.CreatedAt,
                    UpdatedAt = subject.UpdatedAt,
                    Id = subject.Id,
                });
            }

            return new FacetCategoryVM
            {
                FacetCategoryOptionsVMs = facetCategoryOptionsVMs,
                DisplayName = "Series",
                Active = true,
                Slug = "series",
                Id = 0,
            };

        }
    }
}
