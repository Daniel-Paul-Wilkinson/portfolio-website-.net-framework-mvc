﻿using PersonalApp.Core;
using PersonalApp.Core.Enums;
using PersonalApp.Domain;
using PersonalApp.Domain.Entity;
using PersonalApp.Mapper;
using PersonalApp.Services.Interfaces;
using PersonalApp.ViewModels.Design;
using PersonalApp.ViewModels.Image;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PersonalApp.Services.Utility;

namespace PersonalApp.Services.Services
{
    public class ImageService : IImageService
    {
        private readonly IDataContext _context;
        public ImageService(IDataContext context)
        {
            _context = context;
        }

        public async Task<ImageVM> GetImageAsync(int Id)
        {
            var img = await _context.Images.Where(x => x.Id == Id).SingleOrDefaultAsync();
            return img.ToViewModel<ImageVM>();
        }

        public async Task<IEnumerable<ImageVM>> GetImagesAsync()
        {
            var images = await _context.Images.ToListAsync();
            return images.ToViewModel<ImageVM>();
        }

        public async Task<IEnumerable<ImageVM>> GetImageType(ImageTypeEnum imageTypeEnum)
        {
            var images = await _context.Images.Where(x => x.ImageType == (int)imageTypeEnum).ToListAsync();
            return images.ToViewModel<ImageVM>();

        }
        public IQueryable<Image> GetImageQueryAsync()
        {
            //Get inital query
            return _context.Images
                  .AsQueryable<Image>();
        }
        public async Task<DesignPageVM> GetDesignsAsync(IEnumerable<ImageVM> tutorials = null, ImageTypeEnum imageTypeEnum = ImageTypeEnum.Design, FacetedOrderEnum order = FacetedOrderEnum.AlphabeticalAscending, int skip = 0, int pageSize = 0)
        {

            DesignPageVM vm = new DesignPageVM();

            var result = GetImageQueryAsync();

            result = result.Where(x => x.ImageType.Equals((int)imageTypeEnum));

            vm.Count = await result.CountAsync();

            if (!string.IsNullOrEmpty(order.ToString()))
                result = result.OrderByImage(order);
            if (skip > 0)
                result = result.Skip(skip);
            if (pageSize > 0)
                result = result.Take(pageSize);

            var i = await result.ToListAsync();

            vm.FacetItems = i.ToViewModel<ImageVM>();

            return vm;
        }
    }
}
