﻿using Autofac;
using PersonalApp.Core.Interfaces;
using PersonalApp.Domain;
using PersonalApp.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalApp.Services.Services
{
    public class DependencyRegistrar : IDependencyRegistrar
    {
        public void Register(ContainerBuilder builder)
        {

            builder.RegisterType<DataContext>().As<IDataContext>().InstancePerLifetimeScope();
            builder.RegisterType<DataContext>().AsSelf().InstancePerLifetimeScope();

            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            builder.RegisterAssemblyTypes(assemblies)
                .Where(t => t.GetInterfaces().Any(i => i.IsAssignableFrom(typeof(IBaseService))))
                .AsImplementedInterfaces()
                .As<IBaseService>();
        }
    }
}
