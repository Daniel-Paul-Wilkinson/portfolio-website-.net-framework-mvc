﻿using PersonalApp.Domain;
using PersonalApp.Mapper;
using PersonalApp.Services.Interfaces;
using PersonalApp.ViewModels.Facets;
using PersonalApp.ViewModels.Tags;
using PersonalApp.ViewModels.Tutorial;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalApp.Services.Services
{
    public class TagService : ITagService
    {
        private readonly IDataContext _context;
        private readonly ITutorialService _tutorialService;
        private readonly ISubjectService _subjectService;


        public TagService(IDataContext context, ITutorialService tutorialService, ISubjectService subjectService)
        {
            _context = context;
            _tutorialService = tutorialService;
            _subjectService = subjectService;
        }

        public async Task<IEnumerable<TagVM>> GetTagsAsync()
        {
            var tags = await _context.Tags
                .Include(x => x.backgroundColour)
                .Include(x => x.fontColour)
                .Include(x => x.Image)
                .ToListAsync();
            return tags.ToViewModel<TagVM>();
        }
        public async Task<FacetCategoryVM> GetTagFacetCategory(IEnumerable<TutorialVM> tutorials, IEnumerable<string> activeTags)
        {
            List<FacetCategoryOptionsVM> facetCategoryOptionsVMs = new List<FacetCategoryOptionsVM>();

            var subjects = await GetTagsAsync();

            foreach (var subject in subjects)
            {
                facetCategoryOptionsVMs.Add(new FacetCategoryOptionsVM()
                {
                    Active = (activeTags.Contains(subject.Slug)),
                    Slug = subject.Slug,
                    DisplayName = subject.Title,
                    CreatedAt = subject.CreatedAt,
                    UpdatedAt = subject.UpdatedAt,
                    Id = subject.Id,
                });
            }

            return new FacetCategoryVM
            {
                FacetCategoryOptionsVMs = facetCategoryOptionsVMs,
                DisplayName = "Tags",
                Active = true,
                Slug = "tags",
                Id = 0,
            };

        }
    }
}
