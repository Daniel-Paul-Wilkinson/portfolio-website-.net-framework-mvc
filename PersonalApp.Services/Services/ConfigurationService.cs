﻿using PersonalApp.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalApp.Services.Services
{
    public class ConfigurationService : IConfigurationService
    {
        public int ItemsPerPage => GetSetting<int>("Facet:ItemsPerPage");
        public int RelatedCount => GetSetting<int>("Tutorial:RelatedCount");


        private static T GetSetting<T>(string name)
        {
            return (T)Convert.ChangeType(ConfigurationManager.AppSettings[$"PersonalApp:Web:{name}"], typeof(T));
        }
    }
}

