﻿using PersonalApp.Domain;
using PersonalApp.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalApp.Services.Services
{
    public class DesignService : IDesignService
    {
        private readonly IDataContext _context;
        private readonly IImageService _imageService;
        private readonly IConfigurationService _configurationService;

        public DesignService(IDataContext context, IImageService imageService, IConfigurationService configurationService)
        {
            _context = context;
            _imageService = imageService;
            _configurationService = configurationService;
        }

    }
}
