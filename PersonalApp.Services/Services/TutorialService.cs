﻿using PersonalApp.Core.Enums;
using PersonalApp.Domain;
using PersonalApp.Helpers;
using PersonalApp.Mapper;
using PersonalApp.Services.Interfaces;
using PersonalApp.ViewModels.Tutorial;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PersonalApp.Services.Utility;
using PersonalApp.Domain.Entity;
using PersonalApp.ViewModels.PageVM;

namespace PersonalApp.Services.Services
{
    public class TutorialService : ITutorialService
    {
        private readonly IDataContext _context;
        private readonly IImageService _imageService;
        private readonly IConfigurationService _configurationService;

        public TutorialService(IDataContext context, IImageService imageService, IConfigurationService configurationService)
        {
            _context = context;
            _imageService = imageService;
            _configurationService = configurationService;
        }

        public IQueryable<Tutorial> GetTutorialQueryAsync()
        {
            //Get inital query
            return _context.Tutotials
                  .Include(x => x.Image)
                  .Include(x => x.Subject)
                  .Include(x => x.Subject.fontColour)
                  .Include(x => x.Subject.backgroundColour)
                  .Include(x => x.Tags)
                  .Include(x => x.Series)
                  .Include(x => x.Tags.Select(e => e.backgroundColour))
                  .Include(x => x.Tags.Select(e => e.fontColour))
                  .AsQueryable<Tutorial>();
        }


        public async Task<TutorialPageVM> GetTutorialsAsync(IEnumerable<TutorialVM> tutorials = null, string tags = null, string type = null, string series = null, FacetedOrderEnum order = FacetedOrderEnum.AlphabeticalAscending, int skip = 0, int pageSize = 0)
        {

            TutorialPageVM vm = new TutorialPageVM();

            var result = GetTutorialQueryAsync();

            //split lists
            var activeTags = StringHelpers.Split(tags);
            var activeSubjects = StringHelpers.Split(type);
            var activeSeries = StringHelpers.Split(series);


            //get where clause
            if (activeTags != null && activeTags.Count() > 0)
                result = result.Where(x => x.Tags.Any(e => activeTags.Contains(e.Slug.ToLower())));
            if (activeSubjects != null && activeSubjects.Count() > 0)
                result = result.Where(x => activeSubjects.Any(c => c == x.Subject.Slug.ToLower()));
            if (activeSeries != null && activeSeries.Count() > 0)
                result = result.Where(x => activeSeries.Any(c => c == x.Series.Slug.ToLower()));

            //get count of products in query before returning
            vm.Count = await result.CountAsync();
            //pages, order, take and skip
            if (!string.IsNullOrEmpty(order.ToString()))
                result = result.OrderByTutorial(order);
            if (skip > 0)
                result = result.Skip(skip);
            if (pageSize > 0)
                result = result.Take(pageSize);

            //get result as list
            var i = await result.ToListAsync();

            //return mapped tutorials
            vm.Tutorials = i.ToViewModel<TutorialVM>();

            return vm;
        }

        public async Task<int> CountAsync()
        {
            return (await _context.Tutotials.ToListAsync()).Count();
        }

        public async Task<TutorialVM> GetTutorialAsync(int id)
        {
            var tutorial = await _context.Tutotials.Where(x => x.Id == id).SingleOrDefaultAsync();
            return tutorial.ToViewModel<TutorialVM>();
        }

        public async Task<TutorialArticleVM> GetAside(int CurrentArticleId)
        {
            return new TutorialArticleVM
            {
                Aside = new List<TutorialArticleAsideVM>()
                {
                    new TutorialArticleAsideVM()
                    {
                    RealatedTutorials = await GetAssociatedTutorials(CurrentArticleId,TutorialTypeEnum.series),
                    TutorialArticalAsideType = TutorialTypeEnum.series,
                    },
                    new TutorialArticleAsideVM()
                    {
                    RealatedTutorials = await GetAssociatedTutorials(CurrentArticleId,TutorialTypeEnum.similar),
                    TutorialArticalAsideType = TutorialTypeEnum.similar,
                    }
                },

            };
        }

        public async Task<int> ReadTutorial(TutorialVM tutorialVM)
        {
            //someone has read the tutorail.
            tutorialVM.Popularity = tutorialVM.Popularity + 1;

            //return if it was updated.
            return await UpdateTutorial(tutorialVM);
        }

        public async Task<int> UpdateTutorial(TutorialVM tutorialVM)
        {
            //get inital tutorial.
            var tutorial = await _context.Tutotials.FirstOrDefaultAsync(x => x.Id == tutorialVM.Id);

            //map properties - update as nessacary...
            tutorial.Popularity = tutorialVM.Popularity;

           //save changes
           return _context.SaveChanges();
        }




        public async Task<IEnumerable<TutorialVM>> GetAssociatedTutorials(int id, TutorialTypeEnum TutorialTypeEnum)
        {

            var tutorials = await GetTutorialQueryAsync().ToListAsync();

            var exampleTutorial = tutorials.Where(x => x.Id == id).First();

            List<Tutorial> tutorial = null;

            switch (TutorialTypeEnum)
            {
                case TutorialTypeEnum.series:
                    tutorial = tutorials
                    .Where(p => exampleTutorial.Series.Id == p.Series.Id)
                    .Except(new List<Tutorial> { exampleTutorial })
                    .Take(_configurationService.RelatedCount).ToList();
                    break;
                case TutorialTypeEnum.similar:
                    tutorial = tutorials
                    .Where(p => exampleTutorial.Tags.Any(l => p.Tags.Any(c => c == l))
                    || exampleTutorial.Subject.Id.Equals(p.Subject.Id))
                    .Except(new List<Tutorial> { exampleTutorial })
                    .Take(_configurationService.RelatedCount).ToList();
                    break;
            }
            return tutorial.ToViewModel<TutorialVM>();
        }
    }
}
