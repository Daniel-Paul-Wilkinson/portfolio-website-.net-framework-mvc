﻿
using PersonalApp.ViewModels.Facets;
using PersonalApp.ViewModels.Tags;
using PersonalApp.ViewModels.Tutorial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalApp.Services.Interfaces
{
    public interface ITagService : IBaseService
    {
        Task<FacetCategoryVM> GetTagFacetCategory(IEnumerable<TutorialVM> tutorials, IEnumerable<string> activeTags);
        Task<IEnumerable<TagVM>> GetTagsAsync();
    }
}
