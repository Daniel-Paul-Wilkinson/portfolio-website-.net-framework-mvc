﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PersonalApp.ViewModels.Facets;
using PersonalApp.ViewModels.Series;
using PersonalApp.ViewModels.Tutorial;

namespace PersonalApp.Services.Interfaces
{
    public interface ISeriesService : IBaseService
    {
        Task<FacetCategoryVM> GetSeriesCategory(IEnumerable<TutorialVM> tutorials, IEnumerable<string> activeSubjects);
        Task<IEnumerable<SeriesVM>> GetSeriesAsync();
    }
}
