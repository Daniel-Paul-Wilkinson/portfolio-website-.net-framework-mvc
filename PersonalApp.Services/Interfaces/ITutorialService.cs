﻿using PersonalApp.Core.Enums;
using PersonalApp.ViewModels.PageVM;
using PersonalApp.ViewModels.Tutorial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalApp.Services.Interfaces
{
    public interface ITutorialService : IBaseService
    {
        Task<TutorialPageVM> GetTutorialsAsync(IEnumerable<TutorialVM> tutorials = null, string tags = null, string type = null,string series = null, FacetedOrderEnum order = FacetedOrderEnum.AlphabeticalAscending, int skip = 0, int pageSize = 0);
        Task<int> CountAsync();
        Task<TutorialVM> GetTutorialAsync(int id);
        Task<IEnumerable<TutorialVM>> GetAssociatedTutorials(int id, TutorialTypeEnum TutorialTypeEnum);
        Task<TutorialArticleVM> GetAside(int CurrentArticleId);
        Task<int> ReadTutorial(TutorialVM tutorialVM);
        Task<int> UpdateTutorial(TutorialVM tutorialVM);


    }
}
