﻿using PersonalApp.Core;
using PersonalApp.Core.Enums;
using PersonalApp.Domain.Entity;
using PersonalApp.ViewModels.Design;
using PersonalApp.ViewModels.Image;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalApp.Services.Interfaces
{
    public interface IImageService : IBaseService
    {
        // async Task<ImageVM> GetImageAsync(int Id);
        Task<IEnumerable<ImageVM>> GetImageType(ImageTypeEnum imageTypeEnum);
        Task<DesignPageVM> GetDesignsAsync(IEnumerable<ImageVM> tutorials = null, ImageTypeEnum imageTypeEnum = ImageTypeEnum.Design, FacetedOrderEnum order = FacetedOrderEnum.AlphabeticalAscending, int skip = 0, int pageSize = 0);

    }
}
