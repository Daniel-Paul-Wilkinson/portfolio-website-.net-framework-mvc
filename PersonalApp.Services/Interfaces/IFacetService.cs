﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Routing;
using PersonalApp.Core.Enums;
using PersonalApp.ViewModels.Facets;
using PersonalApp.ViewModels.Image;
using PersonalApp.ViewModels.Tutorial;

namespace PersonalApp.Services.Interfaces
{
    public interface IFacetService : IBaseService
    {
        Task<FacetVM> GetTutorialFacet(IEnumerable<TutorialVM> tutorials,string tags, string type, string series);
        FacetedOrderEnum getOrderFromString(string order);
    }
}
