﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalApp.Services.Interfaces
{
    public interface IConfigurationService : IBaseService
    {
        int ItemsPerPage { get; }
        int RelatedCount { get; }
    }
}
