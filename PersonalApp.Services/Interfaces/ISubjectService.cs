﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PersonalApp.ViewModels.Facets;
using PersonalApp.ViewModels.Tutorial;

namespace PersonalApp.Services.Interfaces
{
    public interface ISubjectService : IBaseService
    {
        Task<FacetCategoryVM> GetSubjectFacetCategory(IEnumerable<TutorialVM> tutorials, IEnumerable<string> activeSubjects);
    }
}
