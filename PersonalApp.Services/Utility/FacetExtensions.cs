﻿using PersonalApp.Core.Enums;
using PersonalApp.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalApp.Services.Utility
{
    internal static class FacetExtensions
    {
        public static IQueryable<Ttutorial> OrderByTutorial<Ttutorial>(this IQueryable<Ttutorial> tutorials, FacetedOrderEnum order) where Ttutorial : Tutorial
        {
            switch (order)
            {
                case FacetedOrderEnum.AlphabeticalAscending:
                    return tutorials.OrderBy(p => p.Title);
                case FacetedOrderEnum.AlphabeticalDescending:
                    return tutorials.OrderByDescending(p => p.Title);
                case FacetedOrderEnum.DateAscending:
                    return tutorials.OrderBy(p => p.Published);
                case FacetedOrderEnum.DateDescending:
                    return tutorials.OrderByDescending(p => p.Published);
                case FacetedOrderEnum.Popularity:
                    return tutorials.OrderByDescending(p => p.Popularity);
                default:
                    throw new ArgumentOutOfRangeException(nameof(order), order, null);
            }
        }

        public static IQueryable<TImage> OrderByImage<TImage>(this IQueryable<TImage> Image, FacetedOrderEnum order) where TImage : Image
        {
            switch (order)
            {
                case FacetedOrderEnum.AlphabeticalAscending:
                    return Image.OrderBy(p => p.Title);
                case FacetedOrderEnum.AlphabeticalDescending:
                    return Image.OrderByDescending(p => p.Title);
                case FacetedOrderEnum.DateAscending:
                    return Image.OrderBy(p => p.CreatedAt);
                case FacetedOrderEnum.DateDescending:
                    return Image.OrderByDescending(p => p.CreatedAt);
                default:
                    throw new ArgumentOutOfRangeException(nameof(order), order, null);
            }
        }
    }
}
